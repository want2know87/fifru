<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors".
 *
 * @property integer $fidistr_id
 * @property integer $fitown_id
 * @property integer $ficountry_id
 * @property string $fsname
 * @property string $fsaddress
 * @property string $maps
 * @property string $fssite
 * @property integer $priority
 *
 * @property Country $ficountry
 * @property Region $firegion
 * @property Town $fitown
 * @property DistributorsMail[] $distributorsMails
 * @property DistributorsPhones[] $distributorsPhones
 */
class Distributors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsname'], 'required'],
            [['fitown_id', 'ficountry_id'], 'integer'],
            [['fsname', 'maps', 'fssite'], 'string', 'max' => 255],
            [['fsaddress'], 'string', 'max' => 512],
            ['priority', 'default', 'value' => 0],
            [['priority'], 'integer'],
            [['fsname_en'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistr_id' => Yii::t('app', 'app.internal.number'),
            'fitown_id' => Yii::t('app', 'app.internal.town'),
            'ficountry_id' => Yii::t('app', 'app.internal.country'),
            'fsname' => Yii::t('app', 'app.internal.name'),
            'fsname_en' => Yii::t('app', 'app.internal.name') . ' En',
            'fsaddress' => Yii::t('app', 'app.internal.address'),
            'fssite' => Yii::t('app', 'app.internal.site'),
            'maps' => Yii::t('app', 'app.internal.maps'),
            'distributorsPhones' => Yii::t('app', 'Distributors Phones'),
            'distributorsMails' => Yii::t('app', 'Distributors Mails'),
            'priority' => 'Приоритет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFicountry()
    {
        return $this->hasOne(Country::className(), ['ficountry_id' => 'ficountry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitown()
    {
        return $this->hasOne(Town::className(), ['fitown_id' => 'fitown_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsMails()
    {
        return $this->hasMany(DistributorsMail::className(), ['fidistr_id' => 'fidistr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsPhones()
    {
        return $this->hasMany(DistributorsPhones::className(), ['fidistr_id' => 'fidistr_id']);
    }
}
