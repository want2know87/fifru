<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Distributors;

/**
 * DistributorsSearch represents the model behind the search form about `app\models\Distributors`.
 */
class DistributorsSearch extends Distributors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_id', 'fitown_id', 'ficountry_id'], 'integer'],
            [['fsname', 'fsaddress', 'maps'], 'safe'],
            [['priority'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Distributors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fidistr_id' => $this->fidistr_id,
            'fitown_id' => $this->fitown_id,
            'ficountry_id' => $this->ficountry_id,
            'priority' => $this->priority,
        ]);

        $query->andFilterWhere(['like', 'fsname', $this->fsname])
            ->andFilterWhere(['like', 'fsaddress', $this->fsaddress])
            ->andFilterWhere(['like', 'maps', $this->maps]);

        return $dataProvider;
    }
}
