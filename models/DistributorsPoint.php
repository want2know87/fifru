<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distributors_point".
 *
 * @property integer $fidistr_point_id
 * @property integer $fidistr_id
 * @property string $fsaddress
 * @property string $maps
 * @property string $fssite
 * @property integer $fitown_id
 * @property boolean $distribution_type_wholesale Опт
 * @property boolean $distribution_type_retail Розница
 *
 * @property DistributorsPointMail[] $distributorsPointMails
 * @property DistributorsPointPhones[] $distributorsPointPhones
 */
class DistributorsPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distributors_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fidistr_id'], 'required'],
            [['fidistr_id','fitown_id'], 'integer'],
            [['fsaddress'], 'string', 'max' => 512],
            [['maps', 'fssite'], 'string', 'max' => 255],
            [['distribution_type_retail', 'distribution_type_wholesale'], 'boolean'],
            [['fsaddress_en'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fidistr_point_id' => Yii::t('app', 'Fidistr Point ID'),
            'fidistr_id' => Yii::t('app', 'Fidistr ID'),
            'fsaddress' => Yii::t('app', 'Fsaddress'),
            'fsaddress_en' => Yii::t('app', 'Fsaddress') . ' En',
            'maps' => Yii::t('app', ''),
            'fssite' => Yii::t('app', 'Fssite'),
            'fitown_id' => Yii::t('app', 'app.internal.town'),
            'distribution_type_wholesale' => 'Опт',
            'distribution_type_retail' => 'Розница',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsPointMails()
    {
        return $this->hasMany(DistributorsPointMail::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistributorsPointPhones()
    {
        return $this->hasMany(DistributorsPointPhones::className(), ['fidistr_point_id' => 'fidistr_point_id']);
    }
}
