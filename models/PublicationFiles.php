<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publication_files".
 *
 * @property integer $fipublic_file_id
 * @property integer $fipublic_id
 * @property string $fsname
 * @property string $fsfile
 * @property string $fsreal_name
 *
 * @property Publications $fipublic
 */
class PublicationFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publication_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fipublic_id', 'fsname'], 'required'],
            [['fipublic_id'], 'integer'],
            [['fsname', 'fsfile', 'fsreal_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fipublic_file_id' => Yii::t('app', 'Fipublic File ID'),
            'fipublic_id' => Yii::t('app', 'Fipublic ID'),
            'fsname' => Yii::t('app', 'Fsname'),
            'fsfile' => Yii::t('app', 'Fsfile'),
            'fsreal_name' => Yii::t('app', 'fsreal_name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFipublic()
    {
        return $this->hasOne(Publications::className(), ['fipublic_id' => 'fipublic_id']);
    }
}
