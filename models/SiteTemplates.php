<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_templates".
 *
 * @property integer $fitemplate_id
 * @property string $fstemplate_name
 * @property string $fscode
 * @property string $fstemplate_code
 * @property string $fstemplate_seo
 * @property string $fstitle
 */
class SiteTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fstemplate_code', 'fstemplate_seo'], 'string'],
            [['fstemplate_name', 'fstitle'], 'string', 'max' => 255],
            [['fscode'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fitemplate_id' => Yii::t('app', 'Fitemplate ID'),
            'fstemplate_name' => Yii::t('app', 'Fstemplate Name'),
            'fscode' => Yii::t('app', 'Fscode'),
            'fstemplate_code' => Yii::t('app', 'Fstemplate Code'),
            'fstemplate_seo' => Yii::t('app', 'Fstemplate Seo'),
            'fstitle' => Yii::t('app', 'fstitle'),
        ];
    }
}
