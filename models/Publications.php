<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publications".
 *
 * @property integer $fipublic_id
 * @property string $fsname
 * @property string $fsdesc
 * @property integer $fitype
 * @property string $fsimage
 *
 * @property PublicationFiles[] $publicationFiles
 */
class Publications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsname'], 'required'],
            [['fsdesc'], 'string'],
            [['fitype'], 'integer'],
            [['fsname', 'fsimage'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fipublic_id' => Yii::t('app', 'Fipublic ID'),
            'fsname' => Yii::t('app', 'Fsname'),
            'fsdesc' => Yii::t('app', 'Fsdesc'),
            'fitype' => Yii::t('app', 'Fitype'),
            'fsimage' => Yii::t('app', 'Fsimage'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationFiles()
    {
        return $this->hasMany(PublicationFiles::className(), ['fipublic_id' => 'fipublic_id']);
    }
}
