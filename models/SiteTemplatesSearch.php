<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SiteTemplates;

/**
 * SiteTemplatesSearch represents the model behind the search form about `app\models\SiteTemplates`.
 */
class SiteTemplatesSearch extends SiteTemplates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fitemplate_id'], 'integer'],
            [['fstemplate_name', 'fscode', 'fstemplate_code', 'fstemplate_seo', 'fstitle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteTemplates::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fitemplate_id' => $this->fitemplate_id,
        ]);

        $query->andFilterWhere(['like', 'fstemplate_name', $this->fstemplate_name])
            ->andFilterWhere(['like', 'fscode', $this->fscode])
            ->andFilterWhere(['like', 'fstemplate_code', $this->fstemplate_code])
            ->andFilterWhere(['like', 'fstemplate_seo', $this->fstemplate_seo])
            ->andFilterWhere(['like', 'fstitle', $this->fstitle]);

        return $dataProvider;
    }
}
