<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chpu".
 *
 * @property string $fsyii_link
 * @property string $fsurl
 */
class Chpu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chpu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fsyii_link', 'fsurl'], 'string'],
            [['fsyii_link', 'fsurl'], 'unique', 'targetAttribute' => ['fsyii_link', 'fsurl'], 'message' => 'The combination of Fsyii Link and Fsurl has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fsyii_link' => 'Внутренняя ссылка',
            'fsurl' => 'Ссылка новая',
        ];
    }
}
