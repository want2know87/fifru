<?php

$localParams = is_file(__DIR__ . '/params-local.php')
    ? require(__DIR__ . '/params-local.php')
    : [];

return array_merge([
     'adminEmail' => 'proekt@fif.by',
    //'adminEmail' => 'sl444yer@gmail.com',
    //'adminEmail' => 'xander.sytsevich@gmail.com',
    'googleMapsApiKey' => 'AIzaSyBKo3x7H9_bxGJZHFKORZbSBQvF0BLYfWQ',
    'googleMapsLanguage' => 'ru',
    'googleMapsLibraries' => 'places',
    'fiftestHost' => 'https://fif.by',
], $localParams);
