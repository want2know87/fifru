<?php

$params = require(__DIR__ . '/params.php');

use \yii\web\Request;
$baseUrl = str_replace('/fifru', '', (new Request)->getBaseUrl());
$baseUrl = str_replace('/web', '', $baseUrl);

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'components' => [
        'consoleRunner' => [
            'class' => 'vova07\console\ConsoleRunner',
            'file' => __DIR__ . '/../yii' ,
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array_merge(require(__DIR__ . '/_rules.php'), [
                '' => 'site/index',
                'catalog' => 'catalog/index',
                'distributors' => 'distributors/index',
                'about' => 'site/about',
                'cert' => 'site/certificats',
                'cooperation' => 'site/cooperation',
                'downloads' => 'site/downloads',
                'search' => 'site/search',
                '<language:(ru|geo|abh|en)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<language:(ru|geo|abh|en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<language:(ru|geo|abh|en)>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ]),
        ],
        'i18n'=>array(
            'translations' => array(
                'app' => array(
                    'class' => 'yii\i18n\DbMessageSource',
                )
            ),
        ),
        'view' => [
            'class' => 'app\components\SiteView',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'MQBEdWaDtbYDg4lCHhEofl1B-qkmE9HN',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
