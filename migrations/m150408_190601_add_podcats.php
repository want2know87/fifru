<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_190601_add_podcats extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fishow_with_items', 'int(1) not null default 0');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fishow_with_items');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
