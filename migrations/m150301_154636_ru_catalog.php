<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_154636_ru_catalog extends Migration
{
    public function up()
    {
        $this->createTable('catalog_ru', [
            'ficatalog_id' => 'pk',
            'fsname' => 'varchar(255) not null',
            'fiparent_catalog_id' => 'int(10)',
            'fscatalog_img' => 'varchar(255)',
            'fscatalog_text' => 'varchar(512)',
        ]);
        $this->createIndex('un_catalog_ru_name','catalog_ru', 'fsname', true);
        $this->addForeignKey('fk_catalog_parent','catalog_ru','fiparent_catalog_id','catalog_ru','ficatalog_id');
    }

    public function down()
    {
        $this->dropTable('catalog_ru');
    }
}
