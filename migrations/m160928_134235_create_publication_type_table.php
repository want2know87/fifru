<?php

use yii\db\Schema;
use yii\db\Migration;

class m160928_134235_create_publication_type_table extends Migration
{
    public function up()
    {
        $this->createTable('publication_type', [
            'id' => 'pk',
            'key' => Schema::TYPE_INTEGER . ' NOT NULL',
            'position' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->batchInsert('publication_type',
            ['key', 'position'],
            [
                [0, 0],
                [1, 1],
                [2, 2],
                [3, 3],
                [4, 4],
            ]
        );
    }

    public function down()
    {
        $this->dropTable('publication_type');
    }
}
