<?php

use yii\db\Schema;
use yii\db\Migration;

class m150322_211814_add_manage_show_general_desc extends Migration
{
    public function up()
    {
        $this->addColumn('catalog_ru', 'fishow_general_desc', 'int(1)');
    }

    public function down()
    {
        $this->dropColumn('catalog_ru', 'fishow_general_desc');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
