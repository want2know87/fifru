<?php

use yii\db\Migration;

class m170823_133539_add_en_to_items extends Migration
{
    public function up()
    {
        $this->addColumn('items', 'fsitem_bottom_text_en', $this->text());
        $this->addColumn('items', 'fsitem_small_desc_en', $this->text());
        $this->addColumn('items', 'fsseo_en', $this->text());
    }

    public function down()
    {
        $this->dropColumn('items', 'fsitem_bottom_text_en');
        $this->dropColumn('items', 'fsitem_small_desc_en');
        $this->dropColumn('items', 'fsseo_en');
    }
}
