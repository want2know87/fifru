<?php

use yii\db\Migration;

class m171120_084209_alter_town_en extends Migration
{
    public function safeUp()
    {
        $this->addColumn('town', 'fstown_name_en', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('town', 'fstown_name_en');
    }
}
