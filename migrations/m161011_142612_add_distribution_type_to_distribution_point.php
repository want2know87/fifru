<?php

use yii\db\Schema;
use yii\db\Migration;

class m161011_142612_add_distribution_type_to_distribution_point extends Migration
{
    public function up()
    {
        $this->addColumn('distributors_point', 'distribution_type_retail', Schema::TYPE_BOOLEAN . ' not null default 1');
        $this->addColumn('distributors_point', 'distribution_type_wholesale', Schema::TYPE_BOOLEAN . ' not null default 1');
    }

    public function down()
    {
        $this->dropColumn('distributors_point', 'distribution_type_retail');
        $this->dropColumn('distributors_point', 'distribution_type_wholesale');
    }
}
