<?php

use yii\db\Schema;
use yii\db\Migration;

class m150219_203134_initial_b extends Migration
{
    public function safeUp()
    {
        $this->createTable('distributors', [
            'fidistr_id' => 'pk',
            'fitown_id' => 'int(10) not null',
            'ficountry_id' => 'int(10) not null',
            'firegion_id' => 'int(10) not null',
            'fsname' => 'varchar(255) not null',
            'fsaddress' => 'varchar(512)',
            'maps' => 'varchar(255)',
        ]);

        $this->createIndex('distrib_name', 'distributors', 'fsname');
        $this->createIndex('distrib_town', 'distributors', 'fitown_id');
        $this->createIndex('dest_id', 'distributors', 'firegion_id');
        $this->createIndex('country_id', 'distributors', 'ficountry_id');

        $this->createTable('distributors_mail', [
            'fidistrib_mail_id' => 'pk',
            'fidistr_id' => 'int(10) not null',
            'fsmail' => 'varchar(255)',
        ]);

        $this->addForeignKey('fk_distrib_mail', 'distributors_mail', 'fidistr_id', 'distributors', 'fidistr_id');

        $this->createIndex('distr_mail', 'distributors_mail', 'fidistr_id');
        $this->createIndex('distr_mail_name', 'distributors_mail', ['fidistr_id','fsmail'], true);

        $this->createTable('distributors_phones', [
            'fidistrib_mail_id' => 'pk',
            'fidistr_id' => 'int(10) not null',
            'fsphone' => 'varchar(255)',
        ]);

        $this->addForeignKey('fk_distrib_phone', 'distributors_phones', 'fidistr_id', 'distributors', 'fidistr_id');

        $this->createIndex('distr_phones', 'distributors_phones', 'fidistr_id');
        $this->createIndex('distr_phone_name', 'distributors_phones', ['fidistr_id','fsphone'], true);

        $this->createTable('country', [
            'ficountry_id' => 'pk',
            'fscountry_name' => 'varchar(55)'
        ]);

        $this->addForeignKey('fk_distrib_country', 'distributors', 'ficountry_id', 'country', 'ficountry_id');

        $this->createIndex('u_country_name', 'country', 'fscountry_name', true);

        $this->createTable('region', [
            'firegion_id' => 'pk',
            'fsregion_name' => 'varchar(55)',
            'ficountry_id' => 'int(10) not null',
        ]);

        $this->addForeignKey('fk_region_country', 'region', 'ficountry_id', 'country', 'ficountry_id');
        $this->addForeignKey('fk_distrib_region', 'distributors', 'firegion_id', 'region', 'firegion_id');

        $this->createIndex('u_region_name', 'region', ['ficountry_id', 'fsregion_name'], true);

        $this->createTable('town', [
            'fitown_id' => 'pk',
            'fstown_name' => 'varchar(55)',
            'firegion_id' => 'int(10) not null',
            'ficountry_id' => 'int(10) not null',
        ]);

        $this->addForeignKey('fk_town_country', 'town', 'ficountry_id', 'country', 'ficountry_id');
        $this->addForeignKey('fk_town_region', 'town', 'firegion_id', 'region', 'firegion_id');
        $this->addForeignKey('fk_distrib_town', 'distributors', 'fitown_id', 'town', 'fitown_id');

        $this->createIndex('u_town_name', 'town', ['ficountry_id', 'firegion_id', 'fstown_name'], true);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_distrib_town', 'distributors');
        $this->dropForeignKey('fk_town_region', 'town');
        $this->dropForeignKey('fk_town_country', 'town');

        $this->dropForeignKey('fk_distrib_region', 'distributors');
        $this->dropForeignKey('fk_region_country', 'region');

        $this->dropForeignKey('fk_distrib_country', 'distributors');

        $this->dropForeignKey('fk_distrib_phone', 'distributors_phones');

        $this->dropForeignKey('fk_distrib_mail', 'distributors_mail');

        $this->dropTable('distributors');
        $this->dropTable('town');
        $this->dropTable('region');
        $this->dropTable('country');
        $this->dropTable('distributors_phones');
        $this->dropTable('distributors_mail');
    }
}
