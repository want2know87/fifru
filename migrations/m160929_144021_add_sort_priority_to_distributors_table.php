<?php

use yii\db\Schema;
use yii\db\Migration;

class m160929_144021_add_sort_priority_to_distributors_table extends Migration
{
    public function up()
    {
        $this->addColumn('distributors', 'priority', Schema::TYPE_INTEGER . ' not null default 0');
    }

    public function down()
    {
        $this->dropColumn('distributors', 'priority');
    }
}
