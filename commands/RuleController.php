<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Chpu;

class RuleController extends Controller
{
    const PAT_ITEMS = '@/(catalog/items)\?(id)=(\d+)@';
    const PAT_CAT   = '@/(catalog/index)\?(cat)=(\d+)@';
    const PAT_NEWS  = '@/(news/view)\?(id)=(\d+)@';
    const PAT_FILES = '@/(publication-files/get-item)\?(id)=(\d+)@';

    private function makeRule($chpu)
    {
        $pats = [self::PAT_ITEMS, self::PAT_CAT, self::PAT_NEWS, self::PAT_FILES];

        foreach ($pats as $pat) {
            if (preg_match($pat, $chpu->fsyii_link, $matches)) {
                $route = $matches[1];
                $pattern = $chpu->fsurl;
                $defaults = '["' . $matches[2]  . '" => "'. $matches[3] . '"]';

                return [$pattern, $route, $defaults];
            }
        }

        return [];
    }

    public function actionGenerate()
    {
        $results = [];
        $toHtaccess = [];
        $redirect = function ($route, $query, $pattern) {
            return "\n".join("\n", [
                "RewriteCond %{REQUEST_URI} /$route",
                "RewriteCond %{QUERY_STRING} ^$query",
                "RewriteRule ^.*$ $pattern? [R=301,L]",
            ]);
        };
        foreach (Chpu::find()->each(10) as $chpu) {
            $rule = $this->makeRule($chpu);
            if (empty($rule)) {
                continue;
            }
            list($pattern, $route, $defaults) = $this->makeRule($chpu);
            $res = join("\n", [
                '[',
                    '"pattern" => "' . $pattern . '",',
                    '"route" => "' . $route . '",',
                    '"defaults" => ' . $defaults . ',',
                '],',
            ]);
            // if ($route !== 'publication-files/get-item') {
                $results[] = $res . "\n"; // расскоментировать если что-то пойдет не так
            // }

            $query = str_replace('["', '', $defaults);
            $query = str_replace('" => "', '=', $query);
            $query = str_replace('"]', '', $query);
            $toHtaccess[] = $redirect($route, $query, $pattern);
            //$toHtaccess[] = $redirect($route, $query.'&language=en', '/en'.$pattern);
        }

        $contents = join('', [
            "<?php\n",
            "return [\n",
            join('', $results),
            "];\n",
        ]);
        file_put_contents(__DIR__ . '/../config/_rules.php', $contents);

        try {
            $to = join('', $toHtaccess);
            $htaccessTemplate = file_get_contents(__DIR__ . '/../.htaccess.template');
            $text = str_replace('[[_routes]]', $to, $htaccessTemplate);
            file_put_contents(__DIR__ . '/../.htaccess', $text);
        } catch (\Exception $ex) {

        }

        echo "OK!\n";
    }
}
