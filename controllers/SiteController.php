<?php

namespace app\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ItemsSearch;
use app\models\NewsSearch;
use app\models\CatalogSearchRu;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDownloads() {
        return $this->render('downloads');
    }

    public function actionCooperation()
    {
        $result = false;
        if(Yii::$app->request->isPost) {

            $body = '';
            $body .= 'Имя: ' . "\t" . Yii::$app->request->post('name') . "\n";
            $body .= 'Телефон: ' . "\t" . Yii::$app->request->post('phone') . "\n";
            $body .= 'Страна: ' . "\t" . Yii::$app->request->post('country') . "\n";
            $body .= 'Город: ' . "\t" . Yii::$app->request->post('town') . "\n";
            $body .= 'Email: ' . "\t" . Yii::$app->request->post('mail') . "\n";
            $body .= 'Комментарий: ' . "\t" . Yii::$app->request->post('comment') . "\n";

            $type = Yii::$app->request->post('type');

            try {
                $result = Yii::$app->mailer->compose()
                    ->setTo('2258769@tde-fif.ru')
                    ->setFrom([Yii::$app->request->post('mail') => Yii::$app->request->post('name')])
                    ->setSubject('Заявка на ' . ($type == 'diller' ? 'дилера' : 'инсталлятора'))
                    ->setTextBody($body)
                    ->send();
            }
            catch(Exception $ex) {
                $result = false;
            }

        }
        return $this->render('cooperation', ['result' => $result]);
    }

    public function actionCertificats() {
        return $this->render('sertification');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSearch($query)
    {
        $searchModel = new ItemsSearch();
        @$s['ItemsSearch']['q']=$query;
        $dataProvider = $searchModel->search($s);

        $searchModel2 = new NewsSearch();
        @$s['NewsSearch']['q']=$query;
        $dataProvider2 = $searchModel2->search($s);

        $searchModel3 = new CatalogSearchRu();
        @$s['CatalogSearchRu']['q']=$query;
        $dataProvider3 = $searchModel3->search($s);

        if(Yii::$app->request->isAjax) {
            $response=[];
            if (($search=$dataProvider->getModels())!==null)
                foreach ($search as $row) {
                    $response[]=[
                        'link'=>\yii\helpers\Url::to(['catalog/items', 'id' => $row['fiitem_id']]),
                        'label'=>$row['fsitem_name'],
                        'value'=>$row['fsitem_name'],
                        'query'=>$query,
                    ];
                }
                return json_encode($response);
        } else {
            return $this->render('search',[
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider,
                'dataProvider2'=>$dataProvider2,
                'dataProvider3'=>$dataProvider3,
            ]);
        }
    }
}
