<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AdminController extends \yii\web\Controller
{

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'updatechpu' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        if(\Yii::$app->request->isPost) {
            $file = UploadedFile::getInstancesByName('catalog');
            if($file) {
                $file = current($file);
                $name = 'catalog_' . time() . '_' . 'catalog' . '.' . $file->extension;
                $file->saveAs(dirname(__FILE__).'/../web/' . $name );
            }
            $excel = UploadedFile::getInstanceByName('excel');
            if($excel) {
                $name = 'excel_' . time() . '.' . $excel->extension;
                $excel->saveAs(dirname(__FILE__).'/../web/' . $name );
                $excel = PHPExcel_IOFactory::load(dirname(__FILE__).'/../web/' . $name );
                $articleCol = -1;
                $nameCol    = -1;
                $startRow   = -1;
                $priceRows = [
                    'usd' => [
                        'value' => '',
                        'ind' => 'usd',
                        'col' => -1,
                        'search' => ['usd', '$'],
                    ],
                    'rur' => [
                        'value' => '',
                        'ind' => 'rur',
                        'col' => -1,
                        'search' => ['rur'],
                    ],
                    'byr' => [
                        'value' => '',
                        'ind' => 'byr',
                        'col' => -1,
                        'search' => ['byr'],
                    ],
                ];
                foreach($excel->getWorksheetIterator() as $worksheet) {
                    for($row = 0; $row < $worksheet->getHighestRow(); $row ++) {
                        for($col = 0; $col < PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()); $col++) {
                            $cell = $worksheet->getCellByColumnAndRow($col, $row);
                            if('' == trim($cell->getValue())) {
                                continue;
                            }
                            if('Наименование' == $cell->getValue()) {
                                $startRow = $row + 1;
                                $nameCol  = $col;
                            }
                            if('Артикул' == $cell->getValue()) {
                                $articleCol = $col;
                            }
                            foreach($priceRows as $priceKey => $priceVal) {
                                foreach($priceVal['search'] as $val => $search) {
                                    //echo "Compare ", mb_strtolower($cell->getValue()), ":", $search;
                                    if(strpos(mb_strtolower($cell->getValue()), $search) !== false) {
                                        //echo " finded";
                                        $priceRows[$priceKey]['col'] = $col;
                                        break;
                                    }
                                    //echo "\n";
                                }
                            }
                        }
                        if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                            break;
                        }
                    }

                    if($articleCol >= 0 && $nameCol >= 0 && $startRow >=0) {
                        for($row = $startRow; $row < $worksheet->getHighestRow(); $row++) {
                            $prices = [];
                            foreach($priceRows as $priceRow) {
                                if($priceRow['col'] > 0) {
                                    $prices['fsprice_' . $priceRow['ind']] = $worksheet->getCellByColumnAndRow($priceRow['col'], $row)->getValue();
                                }
                            }
                            //$name = trim($worksheet->getCellByColumnAndRow($nameCol, $row)->getValue());
                            $article = trim($worksheet->getCellByColumnAndRow($articleCol, $row)->getValue());
                            if('' != trim($article)) {
                                $finded = \Yii::$app->db->createCommand('SELECT * FROM items where fsarticle = :name', ['name' => $article])->queryOne();
                                if(!empty($finded)) {
                                    $strPrice = '';
                                    foreach($prices as $key => $value) {
                                        $strPrice .= ' ' . $key . ' = ' . '"' . $value . '", ';
                                    }
                                    \Yii::$app->db->createCommand('UPDATE items SET ' . $strPrice . ' fsarticle = :article WHERE fiitem_id = :itemid', ['article' => $article, 'itemid' => $finded['fiitem_id']])->execute();
                                }
                                /*echo "Finded name: {$name}, article: {$article}, prices: ", json_encode($prices), "\n";*/
                            }

                        }
                    }

                    break;
                }
            }
        }

        return $this->render('index');
    }

    public function actionUpdatechpu()
    {
        Yii::$app->consoleRunner->run('rule/generate');

        return $this->redirect(['index']);
    }

}
