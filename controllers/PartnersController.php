<?php

namespace app\controllers;

use Yii;
use app\models\Partners;
use app\models\PartnersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['admin', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partners();

        $file = UploadedFile::getInstanceByName('Partners[fspartner_image]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file) {
                $file->saveAs(dirname(__FILE__).'/../web/data/img/partner_' . $model->fipartner_id . '.' . $file->extension);
                $model->fspartner_image = 'data/img/partner_' . $model->fipartner_id . '.' . $file->extension;
                $model->save();
            }
            return $this->redirect(['admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldImg = $model->fspartner_image;
        $file = UploadedFile::getInstanceByName('Partners[fspartner_image]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!$file) {
                $model->fspartner_image = $oldImg;
                $model->save();
            }
            else {
                $img = trim($model->fspartner_image);
                if(!empty($img) && file_exists(dirname(__FILE__) . '/../' . $model->fspartner_image)) {
                    unlink(dirname(__FILE__) . '/../web/' . $model->fspartner_image);
                }

                $file->saveAs(dirname(__FILE__).'/../web/data/img/partner_' . $model->fipartner_id . '.' . $file->extension);
                $model->fspartner_image = 'data/img/partner_' . $model->fipartner_id . '.' . $file->extension;
                $model->save();
            }

            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Partners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
