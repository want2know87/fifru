<?php

namespace app\controllers;

use Yii;
use app\models\Items;
use app\models\ItemsSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class ItemsController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['admin', 'create', 'update', 'view', 'delete', 'update-order'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Items models.
   * @return mixed
   */
  public function actionAdmin()
  {
    $searchModel = new ItemsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('admin', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Items model.
   * @param string $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Items model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Items();

    if (Yii::$app->request->isPost) {
      $model->finew = (int)@$_POST['Items']['finew'];
      if((int)@$_POST['Items']['fisale_id'] == 0) {
        $model->fisale_id = null;
      }
      else {
        $model->fisale_id = (int)@$_POST['Items']['fisale_id'];
      }
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {

      $file = UploadedFile::getInstanceByName('Items[fsitem_picture]');

      if ($file) {
        $b = $file->saveAs(dirname(__FILE__) . '/../web/data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension);
        $model->fsitem_picture = 'data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension;
        $model->save();
        $filename = dirname(__FILE__) . '/../web/data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension;
        list($width, $height) = getimagesize($filename);
        $newwidth = 280;
        $newheight = $height/($width/$newwidth);
        if(($width > 1000 || $height > 1000) && in_array($file->extension,['jpg','jpeg', 'png'])) {
          $thumb = imagecreatetruecolor($newwidth, $newheight);
          switch($file->extension) {
            case 'jpg':
            case 'jpeg':
              $source = imagecreatefromjpeg($filename);
              break;
            case 'png':
              $source = imagecreatefrompng($filename);
              break;
          }

          imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

          imagejpeg($thumb, $filename . '.small.jpg', 100);
        }
      }

      $file = UploadedFile::getInstanceByName('Items[fspassport]');

      if ($file) {
        $file->saveAs(dirname(__FILE__) . '/../web/data/files/item_' . $model->fiitem_id . '.ru.' . $file->extension);
        $model->fspassport = 'data/files/item_' . $model->fiitem_id . '.ru.' . $file->extension;
        $model->save();
      }

      $this->saveProps($model);
      $this->saveCats($model);

      return $this->redirect(['admin', 'page' => Yii::$app->request->post('page')]);
    } else {
      return $this->render('create', [
        'model' => $model,
        'itemList' => $this->getItemList(),
      ]);
    }
  }

  /**
   * @param Items $model
   * @throws Exception
   * @throws \Exception
   * @throws \yii\db\Exception
   */
  private function saveCats(Items $model)
  {
    $cats = Yii::$app->request->post('cats', []);
    $cats = array_map('intval', $cats);
    $transaction = Yii::$app->db->beginTransaction();
    try {
      Yii::$app->db->createCommand('delete from catalog_items where fiitem_id = :itemid', ['itemid' => $model->fiitem_id])->execute();
      foreach ($cats as $cat) {
        Yii::$app->db->createCommand('insert into catalog_items(ficatalog_id, fiitem_id) values(:catalogid, :itemid)', ['catalogid' => $cat, 'itemid' => $model->fiitem_id])->execute();
      }
    } catch (Exception $ex) {
      $transaction->rollBack();
      throw $ex;
    }
    $transaction->commit();
  }

  /**
   * @param Items $model
   * @throws Exception
   * @throws \Exception
   * @throws \yii\db\Exception
   */
  private function saveProps(Items $model)
  {
    $props = Yii::$app->request->post('item-props', '');
    $props = explode("\n", $props);
    $props = array_map('trim', $props);
    $transaction = Yii::$app->db->beginTransaction();
    $index = 0;
    try {
      Yii::$app->db->createCommand('delete from params_values where fiitem_id = :item', ['item' => $model->fiitem_id])->execute();
      foreach ($props as $prop) {
        if ('' == trim($prop)) {
          continue;
        }
        $prop = explode("|", $prop);
        if (count($prop) != 2) {
          continue;
          /*$transaction->rollBack();
          throw new Exception('Fail to save prop');*/
        } else {
          $prop = array_map('trim', $prop);
          $propId = Yii::$app->db->createCommand('select fiparam_id from params_cat pc where pc.fsparam_name = :name', ['name' => trim($prop[0])])->queryOne();
          if (empty($propId)) {
            Yii::$app->db->createCommand('insert into params_cat(fsparam_name, fiparam_show_filter) values(:pname, 0)', ['pname' => $prop[0]])->execute();
            $propId = Yii::$app->db->getLastInsertID();
          } else {
            $propId = $propId['fiparam_id'];
          }
          Yii::$app->db->createCommand('insert into params_values(fiitem_id,fiparam_id,fsparam_value,fiorder) values(:item,:param,:value,:order)', [
            'item' => $model->fiitem_id,
            'param' => $propId,
            'value' => $prop[1],
            'order' => $index++,
          ])->execute();
        }
      }
    } catch (Exception $ex) {
      $transaction->rollBack();
      throw $ex;
    }
    $transaction->commit();
  }

  /**
   * Updates an existing Items model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param string $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $oldImg = $model->fsitem_picture;
    $oldPassport = $model->fspassport;
    if (Yii::$app->request->isPost) {
      $model->finew = (int)@$_POST['Items']['finew'];
      if((int)@$_POST['Items']['fisale_id'] == 0) {
        $model->fisale_id = null;
      }
      else {
        $model->fisale_id = (int)@$_POST['Items']['fisale_id'];
      }
    }

    $file = UploadedFile::getInstanceByName('Items[fsitem_picture]');
    $passport = UploadedFile::getInstanceByName('Items[fspassport]');

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      if (!$file) {
        $model->fsitem_picture = $oldImg;
        $model->save();
      } else {
        $filename = dirname(__FILE__) . '/../web/' . $oldImg;
        if (!empty($oldImg) && file_exists($filename)) {
          unlink($filename);
        }

        $b = $file->saveAs(dirname(__FILE__) . '/../web/data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension);
        $model->fsitem_picture = 'data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension;
        $model->save();
        $filename = dirname(__FILE__) . '/../web/data/img/item_' . $model->fiitem_id . '.ru.' . $file->extension;
        list($width, $height) = getimagesize($filename);
        $newwidth = 280;
        $newheight = $height/($width/$newwidth);
        if(($width > 1000 || $height > 1000) && in_array($file->extension,['jpg','jpeg', 'png'])) {
          $thumb = imagecreatetruecolor($newwidth, $newheight);
          switch($file->extension) {
            case 'jpg':
            case 'jpeg':
              $source = imagecreatefromjpeg($filename);
              break;
            case 'png':
              $source = imagecreatefrompng($filename);
              break;
          }

          imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

          imagejpeg($thumb, $filename . '.small.jpg', 100);
        }
      }

      if (!$passport) {
        $model->fspassport = $oldPassport;
        $model->save();
      } else {
        $img = trim($model->fspassport);
        if (!empty($img) && file_exists(dirname(__FILE__) . '/../' . $model->fspassport)) {
          unlink(dirname(__FILE__) . '/../web/' . $model->fspassport);
        }

        $passport->saveAs(dirname(__FILE__) . '/../web/data/files/item_' . $model->fiitem_id . '.ru.' . $passport->extension);
        $model->fspassport = 'data/files/item_' . $model->fiitem_id . '.ru.' . $passport->extension;
        $model->save();
      }

      $this->saveProps($model);
      $this->saveCats($model);

      return $this->redirect(['admin', 'page' => Yii::$app->request->post('page')]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'itemList' => $this->getItemList($model->fiitem_id),
      ]);
    }
  }

  /**
   * Deletes an existing Items model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param string $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $model = $this->findModel($id);
    Yii::$app->db->createCommand('delete from items_pictures where fiitem_id = :itemid', ['itemid' => $model->fiitem_id])->execute();
    Yii::$app->db->createCommand('delete from catalog_items where fiitem_id = :itemid', ['itemid' => $model->fiitem_id])->execute();
    Yii::$app->db->createCommand('delete from params_values where fiitem_id = :item', ['item' => $model->fiitem_id])->execute();
    $model->delete();

    return $this->redirect(['admin']);
  }

  /**
   * Finds the Items model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param string $id
   * @return Items the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Items::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionUpdateOrder($id, $order)
  {
    $model = $this->findModel($id);
    $model->fiorder = (int)$order;
    $model->save();
  }

  private function getItemList($selfId = null)
    {
        return Items::find()
            ->andWhere(['<>', 'fiitem_id', $selfId])
            ->orderBy('fsitem_name')
            ->all();
    }
}
