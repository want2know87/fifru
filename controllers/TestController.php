<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Json;

class TestController extends Controller
{
    private $fiftestHost;

    public function init()
    {
        parent::init();
        $this->fiftestHost = Yii::$app->params['fiftestHost'];
    }

    public function actionItem()
    {
        $data = [];
        $q = Yii::$app->request->post('q');
        if ($q) {
            $data = $this->getItem($q);
        }
        return $this->render('/catalog/_test-form', [
            'data' => $data,
        ]);
    }

    public function actionSuggest($q = null)
    {
        $list = [];
        $list = $this->httpPost($this->fiftestHost . '/fiftest/?r=api/getitem&q='.$q);
        $data = array_map(function ($item) {
            return ['value' => $item];
        }, Json::decode($list));

        echo Json::encode($data);
    }

    private function getSuggestionList()
    {

    }

    private function getItem($q)
    {
        $data = [];
        $data = $this->httpPost($this->fiftestHost . '/fiftest/?r=api/index', ['search_text' => $q]);
        return Json::decode($data);
    }

    private function httpPost($url, $params = [])
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach ($params as $k => $v) {
            $postData .= $k . '='.$v.'&';
        }
        $postData = rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);
        if($output === false) {
            // echo "Error Number:".curl_errno($ch)."<br>";
            // echo "Error String:".curl_error($ch);
            // $output = [
            //     'Error Number' => curl_errno($ch),
            //     'Error String' => curl_error($ch)
            // ];
        }

        curl_close($ch);
        return $output;
    }
}
