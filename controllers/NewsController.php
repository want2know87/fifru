<?php

namespace app\controllers;

use Yii;
use app\models\News;
use app\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                [
                  'actions' => ['admin', 'create', 'update', 'delete', 'index', 'view'],
                  'allow' => true,
                  'roles' => ['@'],
                ],
                [
                  'actions' => ['index', 'view'],
                  'allow' => true,
                  'roles' => ['?'],
                ],
              ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
      $searchModel = new NewsSearch();
      /*$dataProvider = $searchModel->search(Yii::$app->request->queryParams);*/

      return $this->render('index', [
        'searchModel' => $searchModel,
        /*'dataProvider' => $dataProvider,*/
      ]);
    }

    /**
     * Displays a single News model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model) {
          $this->view->seoFields = @$model->fsseo;
        }
        return $this->render('view-news', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        $file = UploadedFile::getInstanceByName('News[fdtitle_image]');
        $file2 = UploadedFile::getInstanceByName('News[fsimage_news]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($file) {
              $file->saveAs(dirname(__FILE__).'/../web/data/news/news_' . $model->finewsid . '.' . $file->extension);
              $model->fdtitle_image = 'data/news/news_' . $model->finewsid . '.' . $file->extension;
              $model->save();
            }
            if($file2) {
              $file2->saveAs(dirname(__FILE__).'/../web/data/news/news_n_' . $model->finewsid . '.' . $file2->extension);
              $model->fsimage_news = 'data/news/news_n_' . $model->finewsid . '.' . $file2->extension;
              $model->save();
            }
            return $this->redirect(['admin', 'id' => $model->finewsid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldImage = $model->fdtitle_image;
        $oldImage2 = $model->fsimage_news;
        $file = UploadedFile::getInstanceByName('News[fdtitle_image]');
        $file2 = UploadedFile::getInstanceByName('News[fsimage_news]');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(empty($file)) {
              $model->fdtitle_image = $oldImage;
              $model->save();
            }
            else {
              if(file_exists(dirname(__FILE__).'/../web/data/news/news_' . $model->finewsid . '.' . $file->extension)) {
                unlink(dirname(__FILE__).'/../web/data/news/news_' . $model->finewsid . '.' . $file->extension);
              }
              $file->saveAs(dirname(__FILE__).'/../web/data/news/news_' . $model->finewsid . '.' . $file->extension);
              $model->fdtitle_image = 'data/news/news_' . $model->finewsid . '.' . $file->extension;
              $model->save();
            }
            if(empty($file2)) {
              $model->fsimage_news = $oldImage2;
              $model->save();
            }
            else {
              if(file_exists(dirname(__FILE__).'/../web/data/news/news_n_' . $model->finewsid . '.' . $file2->extension)) {
                unlink(dirname(__FILE__).'/../web/data/news/news_n_' . $model->finewsid . '.' . $file2->extension);
              }
              $file2->saveAs(dirname(__FILE__).'/../web/data/news/news_n_' . $model->finewsid . '.' . $file2->extension);
              $model->fsimage_news = 'data/news/news_n_' . $model->finewsid . '.' . $file2->extension;
              $model->save();
            }
            return $this->redirect(['admin', 'id' => $model->finewsid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
