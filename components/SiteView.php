<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 20.02.2015
 * Time: 20:54
 */

namespace app\components;

use yii\web\View;


class SiteView extends View {

    /**
     * @var bool
     */
    private $_showBanner = false;

    public $seoFields = '';

    /**
     * @param $value
     */
    public function setShowBanner($value) {
        $this->_showBanner = $value;
    }

    public function getShowBanner() {
        return $this->_showBanner;
    }

}