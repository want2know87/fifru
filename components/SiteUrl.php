<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use Yii;
use yii\web\UrlManager;

class SiteUrl extends UrlManager {

    /**
     * @param array|string $params
     * @return string
     */
    public function createUrl($params) {
        /*$params['language'] = Yii::$app->language;*/
        $result = parent::createUrl($params);
        $find = str_replace(Yii::$app->request->baseUrl, '', $result);

        $alias = \app\models\Chpu::find()->where(['fsyii_link' => $find])->all();
        if(!empty($alias)) {
            $result = Yii::$app->request->baseUrl . $alias[0]['fsurl'];
        }

        return $result;
    }

    /**
     * @param \yii\web\Request $request
     * @return array|bool
     */
    public function parseRequest($request) {
        $info = trim('/' . trim($request->getPathInfo(),'/') . '?' . $request->queryString, '?');


        $alias = \app\models\Chpu::find()->where(['fsyii_link' => $info])->orWhere(['fsurl' => $info])->all();
        if (!$alias && strpos($request->getPathInfo(),'/catalog/')!==false){

            if(strpos($info, '?') !== false) {
              $info = substr($info, 0, strpos($info, '?'));
            }
            $alias = \app\models\Chpu::find()->where(['fsyii_link' => $info])->orWhere(['fsurl' => $info])->all();

        }
        if(!empty($alias)) {
            if($info == $alias[0]['fsyii_link']) {
                if($info != $alias[0]['fsurl']) {
                    Yii::$app->response->redirect(Yii::$app->request->baseUrl . $alias[0]['fsurl'], 301);
                    Yii::$app->end();
                }
            }
            else {
                $query = explode('?', $alias[0]['fsyii_link']);
                $info = $query[0];
                $request->setPathInfo($info);
                if(isset($query[1])) {
                    $params = explode('&', $query[1]);
                    $paramArray = [];
                    foreach($params as $param) {
                        $par = explode('=', $param);
                        $paramArray[@$par[0]] = @$par[1];
                    }
                    $request->setQueryParams($paramArray);
                }
            }  
        }

        $result = parent::parseRequest($request);
        /*$lang = 'ru';
        foreach($result as $res) {
            if(is_array($res)) {
                foreach($res as $key => $val) {
                    if('language' == $key) {
                        if(in_array($val, ['ru', 'en', 'abh', 'geo'])) {
                            $lang = $val;
                            break;
                        }
                    }
                }
            }
        }
        Yii::$app->language = $lang;*/
        return $result;
    }

}