<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OwlAsset extends AssetBundle
{
    // public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'owl-carousel/owl.carousel.css',
        'owl-carousel/owl.theme.css',
    ];
    public $js = [
        'owl-carousel/owl.carousel.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function registerAssetFiles($view)
    {

        $view->registerJs("

            $(document).ready(function() {
             
              var owl = $(\"#owl-newitem\");
             
              owl.owlCarousel({
                 
                  itemsCustom : [
                    [0, 1],
                    [767, 2],
                    [992, 3]
                  ],
                  margin:10,
                  navigation : false,
                  pagination : false
             
              });
             $(\".flex-next\").click(function(){
                owl.trigger('owl.next');
                return false;
              });
              $(\".flex-prev\").click(function(){
                owl.trigger('owl.prev');
                return false;
              });
            });

        ");

        parent::registerAssetFiles($view);
    }

}
