<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\SiteView;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;

/* @var $this SiteView */
/* @var $content string */

AppAsset::register($this);

$js = <<< JS

$(function() {
    $('.container > .main-menu').on('click', '> div', function() {
        window.location.href = $(this).find('a').attr('href');
        return false;
    });

    $('.general-logo .flag').on('click', '.btn-group .dropdown-menu li', function() {
        /*var lang = $(this).data('lang');*/
        var action = $(this).data('link');
        var langName = $(this).text();
        $(this).closest('.btn-group').find('.btn').html(langName + ' <span class="caret"></span>');
        $(this).closest('form').attr('action', action);
        $(this).closest('form').submit();
        /*$(this).closest('.price-cell').find('.number').text(price);*/
    });
});

JS;

$this->registerJs($js);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl ?>/image/site/fav.jpg" type="image/jpeg">
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
<div class="full-container">
<div class="general-logo">
    <div class="container">
        <div class="col-xs-12 padding-none">
            <div class="col-xs-5 padding-none">
                <a href="<?php echo Yii::$app->request->baseUrl ?>" class="logo"><img
                        src="<?php echo Yii::$app->request->baseUrl . '/image/site/logo_new.gif' ?>"></a><br/>

                <div class="general-title">
                    <?php echo Yii::t('app', 'general.banner.text'); ?>
                </div>
            </div>
            <div class="col-xs-7 padding-none title-menu">
                <div class="col-xs-11 padding-none">
                    <div class="col-xs-4 padding-none">
                                <span class="gray"><?php echo Yii::t('app', 'general.title-tel.russia') ?></span><br/>
                                <span class="g-link-a"><a href="tel:<?php echo Yii::t('app', 'general.title-tel.russia-tel') ?>"><?php echo Yii::t('app', 'general.title-tel.russia-tel') ?></a></span>
                    </div>
                    <div class="col-xs-4 padding-none">
                                <span class="gray"><?php echo Yii::t('app', 'general.title-tel.moscow') ?></span><br/>
                                <span class="g-link-a"><a href="tel:+<?php echo Yii::t('app', 'general.title-tel.moscow-tel') ?>"><?php echo Yii::t('app', 'general.title-tel.moscow-tel') ?></a></span>
                    </div>
                    <div class="col-xs-4 padding-none">
                                <span class="gray"><?php echo Yii::t('app', 'general.title-inf.title') ?></span><br/>
                                <span class="g-link-a"><a href="mailto:<?php echo Yii::t('app', 'general.title-inf.title-mail') ?>"><?php echo Yii::t('app', 'general.title-inf.title-mail') ?></a></span>
                    </div>
                </div>
                <div class="col-xs-1 padding-none">
                    <div class="flag">
                        <form method="get" action="<?php echo Url::to(['site/change-lang']) ?>">
                        <?php
                        echo ButtonDropdown::widget([
                            'label' => ucfirst(Yii::$app->language),
                            'options' => [
                                'class' => 'btn-lg btn-default',
                            ],
                            'dropdown' => [
                                'items' => [
                                    [
                                        'label' => 'Ru',
                                        'options' => [
                                            'data-lang' => 'ru',
                                            'data-link' => str_replace('/' . Yii::$app->language . '/','/ru/', Yii::$app->request->url),
                                        ]
                                    ], [
                                        'label' => 'En',
                                        'options' => [
                                            'data-lang' => 'en',
                                            'data-link' => str_replace('/' . Yii::$app->language . '/','/en/', Yii::$app->request->url),
                                        ]], [
                                        'label' => 'Geo',
                                        'options' => [
                                            'data-lang' => 'geo',
                                            'data-link' => str_replace('/' . Yii::$app->language . '/','/geo/', Yii::$app->request->url),
                                        ]],
                                    [
                                        'label' => 'Abh',
                                        'options' => [
                                            'data-lang' => 'abh',
                                            'data-link' => str_replace('/' . Yii::$app->language . '/','/abh/', Yii::$app->request->url),
                                        ]],
                                ],
                            ],
                        ]);
                        ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="seven-cols main-menu">
        <div
            class="col-xs-1 padding-none <?php echo (Yii::$app->request->url == Url::to(['site/index']) || Yii::$app->request->url == Yii::$app->request->baseUrl . '/') ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.general'), Url::to(['site/index'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['catalog/index']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.catalog'), Url::to(['catalog/index'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['site/cooperation']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.cooperation'), Url::to(['site/cooperation'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['site/about']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.about'), Url::to(['site/about'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['site/certificats']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.certificats'), Url::to(['site/certificats'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['site/downloads']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.downloads'), Url::to(['site/downloads'])) ?></div>
        <div
            class="col-xs-1 padding-none <?php echo Yii::$app->request->url == Url::to(['distributors/index']) ? 'active' : ''; ?>"><?php echo Html::a(Yii::t('app', 'navigation.distributors'), Url::to(['distributors/index'])) ?></div>
    </div>
</div>

<?php if ($this->getShowBanner()): ?>
    <div class="top-banner">
        <div class="container">
            <?php echo Yii::t('app', 'general.tag-line') ?>
        </div>
    </div>
<?php endif; ?>

<div class="container">
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= $content ?>
</div>

<?php if ($this->getShowBanner()): ?>
    <div class="bottom-banner">
        <div class="col-xs-12">
            <div class="col-xs-1"></div>
            <div class="col-xs-5">
                <?php echo Yii::t('app', 'general.find-text') ?>

                <div class="general-button"><?php echo Yii::t('app', 'general.find') ?></div>
            </div>
            <div class="col-xs-6">
                <?php echo Yii::t('app', 'general.select-text') ?>

                <div class="general-button"><?php echo Yii::t('app', 'general.select') ?></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->getShowBanner()): ?>
    <div class="container">
        <div class="col-xs-12 evr-this center-align">
            <?php echo Yii::t('app', 'general.euroautomatik.this') ?>
        </div>

        <div class="col-xs-12 evr-this-desc padding-none">
            <div class="col-xs-4 center-align">
                <div class="col-xs-12 padding-none image">
                    <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/this1.png') ?>
                </div>
                <div class="col-xs-12 padding-none title">
                    <?php echo Yii::t('app', 'general.euro.capasity') ?>
                </div>
                <div class="col-xs-12 padding-none desc">
                    <?php echo Yii::t('app', 'general.euro.capasity-text') ?>
                </div>
            </div>
            <div class="col-xs-4 center-align">
                <div class="col-xs-12 padding-none image">
                    <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/this2.png') ?>
                </div>
                <div class="col-xs-12 padding-none title">
                    <?php echo Yii::t('app', 'general.euro.price') ?>
                </div>
                <div class="col-xs-12 padding-none desc">
                    <?php echo Yii::t('app', 'general.euro.price-text') ?>
                </div>
            </div>
            <div class="col-xs-4 center-align">
                <div class="col-xs-12 padding-none image">
                    <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/this3.png') ?>
                </div>
                <div class="col-xs-12 padding-none title">
                    <?php echo Yii::t('app', 'general.euro.time') ?>
                </div>
                <div class="col-xs-12 padding-none desc">
                    <?php echo Yii::t('app', 'general.euro.time-text') ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 evr-this center-align">
            <?php echo Yii::t('app', 'general.partners') ?>
        </div>

        <div class="col-xs-12 center-align general-partners">
            <?php $partners = \app\models\Partners::find()->all(); ?>
            <?php foreach ($partners as $partner): ?>
                <div class="col-xs-3 padding-none">
                    <div class="col-xs-12 padding-none image">
                        <?php echo Html::a(Html::img(Yii::$app->request->baseUrl . '/' . $partner->fspartner_image, ['alt' => $partner->fspartner_name, 'title' => $partner->fspartner_name]), $partner->fspartner_link) ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-xs-12 evr-this center-align">
        </div>
    </div>
<?php endif; ?>

</div>
</div>

<footer class="footer">
    <div class="container">
        <div class="bottom-container col-xs-12">
            <div class="col-xs-4 padding-none">
                <div class="col-xs-12 bottom-line logo padding-none">
                    <a href="<?php echo Yii::$app->request->baseUrl ?>">
                        <img src="<?php echo Yii::$app->request->baseUrl ?>/image/site/bottom-logo.png">
                    </a>
                </div>
                <div class="col-xs-12 bottom-text padding-none text-all">
                    <?php echo Yii::t('app', 'footer.legend') ?>
                </div>
                <div class="col-xs-12 bottom-text padding-none evroavt"><?php echo Yii::t('app', 'footer.brand') ?>
                    2008-<?php echo date('Y'); ?></div>
            </div>
            <div class="col-xs-4">
                <div class="col-xs-12 bottom-line info-block padding-none">
                    <?php echo Yii::t('app', 'footer.cats') ?>
                </div>
                <div class="col-xs-12 bottom-text padding-none">
                    <div class="col-xs-6 padding-none">
                        <?php echo Html::a(Yii::t('app', 'navigation.catalog'), Url::to(['catalog/index'])) ?><br/>
                        <?php echo Html::a(Yii::t('app', 'navigation.cooperation'), Url::to(['site/cooperation'])) ?><br/>
                        <?php echo Html::a(Yii::t('app', 'navigation.about'), Url::to(['site/about'])) ?><br/>
                    </div>
                    <div class="col-xs-6 padding-none">
                        <?php echo Html::a(Yii::t('app', 'navigation.certificats'), Url::to(['site/certificats'])) ?><br/>
                        <?php echo Html::a(Yii::t('app', 'navigation.downloads'), Url::to(['site/download'])) ?><br/>
                        <?php echo Html::a(Yii::t('app', 'navigation.distributors'), Url::to(['distributors/index'])) ?><br/>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 padding-none">
                <div class="col-xs-12 bottom-line info-block padding-none">
                    <?php echo Yii::t('app','footer.inf') ?>
                </div>
                <div class="col-xs-12 bottom-text padding-none">
                    <div class="col-xs-7 padding-none">
                        <?php echo Yii::t('app', 'general.title-tel.russia') ?><br/>
                        <?php echo Yii::t('app', 'general.title-tel.moscow') ?><br/>
                        <?php echo Yii::t('app', 'general.title-inf.title') ?><br/>
                    </div>
                    <div class="col-xs-5 padding-none">
                        <a href="tel:<?php echo Yii::t('app', 'general.title-tel.russia-tel') ?>"><?php echo Yii::t('app', 'general.title-tel.russia-tel') ?></a><br/>
                        <a href="tel:<?php echo Yii::t('app', 'general.title-tel.moscow-tel') ?>"><?php echo Yii::t('app', 'general.title-tel.moscow-tel') ?></a><br/>
                        <a href="mailto:<?php echo Yii::t('app', 'general.title-inf.title-mail') ?>"><?php echo Yii::t('app', 'general.title-inf.title-mail') ?></a><br/>
                    </div>
                    <div class="col-xs-12 developer padding-none">
                        <?php echo Yii::t('app', 'footer.develop') ?> <a href="http://abbey.by">ABBEY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
