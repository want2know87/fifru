<?php

use app\models\Sales;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CatalogRu;
use app\models\CatalogItems;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Items */
/* @var $form yii\widgets\ActiveForm */

$params = Yii::$app->db->createCommand('select CONCAT(pc.fsparam_name,"|",pv.fsparam_value) as prop from params_values pv left JOIN params_cat pc on pc.fiparam_id = pv.fiparam_id where pv.fiitem_id = :id order by pv.fiorder',['id' => (int)$model->fiitem_id])->queryAll();
$props = '';
foreach($params as $param) {
    $props .= $param['prop'] . "\n";
}

$parent = [];
$parentsQeury = CatalogRu::find()/*->where('fiparent_catalog_id IS NOT NULL')*/->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

$cats = [];
$catsItem = CatalogItems::find()->where(['fiitem_id' => $model->fiitem_id])->all();
/** @var CatalogItems $ci */
foreach($catsItem as $ci) {
    $cats[] = $ci->ficatalog_id;
}

$new = [0 => 'Нет', 1 => 'Да'];

$listSales = [
  0 => 'Нет'] + ArrayHelper::map(Sales::find()->all(),'fisale_id', 'fssale_name');

$js = <<< JS
$(function() {
    $('.items-form .catalogs').on('click', 'div .g-link', function() {
        $(this).closest('div').remove();
    });

    $('.items-form').on('click','.add-catalog', function() {
        if($('.catalogs').find('div[data-id=' + $('#current-catalog').val() + ']').length) {
            return false;
        }
        $('<div/>')
        .attr('data-id', $('#current-catalog').val())
        .append(
            $('<span/>')
                .addClass('name')
                .text($('#current-catalog :selected').text())
        )
        .append(
            $('<span/>')
                .addClass('g-link padding-left')
                .text('Удалить категорию')
        )
        .append(
            $('<input/>')
                .attr({'type' : 'hidden', 'name' : 'cats[]', 'value' : $('#current-catalog').val()})
        )
        .insertBefore($('#current-catalog'));
        return false;
    });
});

/*$(function() {
        $('#items-fsitem_picture').picEdit();
    });*/

JS;

$this->registerJs($js);


$this->registerJsFile('@web/js/ckeditor/ckeditor.js');
$this->registerJs("
$(function() {
        CKEDITOR.replace('items-fsitem_bottom_text',
            {
                height  : '400px',
                on: {
                instanceReady:function( ev ){
                   jQuery(CKEDITOR.instances['items-fsitem_bottom_text'].container.$).mouseleave(function() {
                      CKEDITOR.instances['items-fsitem_bottom_text'].updateElement();
                   });
                   CKEDITOR.instances['items-fsitem_bottom_text'].on('blur', function() {
                      CKEDITOR.instances['items-fsitem_bottom_text'].updateElement();
                   });
                }
             }
            });
        $('#items-fsitem_bottom_text').on('change', function() {
          CKEDITOR.instances['items-fsitem_bottom_text'].setData($(this).val());
        });
});            ");

// $this->registerJsFile('@web/js/cropper/jquery.imgareaselect.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<div class="items-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <input type="hidden" name="page" value="<?php echo Yii::$app->request->get('page'); ?>" />

    <?= $form->field($model, 'fsitem_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsarticle')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'fsprice_rur')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'fsprice_usd')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'fsprice_byr')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'fsitem_before_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsitem_picture')->fileInput() ?>

    <?php if(strlen($model->fsitem_picture)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsitem_picture, ['style' => 'max-width:200px;max-height:200px;']); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'fspassport')->fileInput() ?>

    <?php if(strlen($model->fspassport)): ?>
        <div class="form-group">
            <?php echo Html::a(Yii::t('app', 'fspassport'),Yii::$app->request->baseUrl . '/' . $model->fspassport); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'fsitem_passport_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsitem_small_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsitem_small_desc_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fsseo')->textarea() ?>

    <?= $form->field($model, 'fsseo_en')->textarea() ?>

    <div class="form-group">
        <label class="control-label" for="items-props">Свойства товара (Название|Значение)</label>
        <?= Html::textarea('item-props', $props, ['class' => 'form-control', 'rows' => 10]) ?>
    </div>

    <div class="form-group catalogs">
        <?php foreach($cats as $cat): ?>
          <?php if(isset($parent[$cat])): ?>
            <div data-id="<?php echo $cat ?>">
                <span class="name"><?php echo $parent[$cat] ?></span><span class="g-link padding-left">Удалить категорию</span><input value="<?php echo $cat ?>" name="cats[]" type="hidden">
            </div>
          <?php endif;?>
        <?php endforeach; ?>
        <?php echo Html::dropDownList('active-cats', null, $parent, ['id' => 'current-catalog', 'class' => 'form-control', 'style' => 'display:inline;width:400px;']) ?>
        <?php echo Html::button(Yii::t('app', 'add.to.cat'), ['class' => 'btn add-catalog']) ?>
    </div>

    <div class="form-group">
      <?php echo $form->field($model, 'finew')->dropDownList($new) ?>
    </div>

    <div class="form-group">
        <?php echo $form->field($model, 'fisale_id')->dropDownList($listSales); ?>
    </div>

    <div class="form-group">
        <?php echo $form->field($model, 'fsitem_bottom_text')->textarea(); ?>
    </div>

    <div class="form-group">
        <?php echo $form->field($model, 'fsitem_bottom_text_en')->textarea(); ?>
    </div>

    <?php if(!$model->isNewRecord): ?>
    <?php
        $itemModel = $model;
        $query = \app\models\ItemsPictures::find()->where(['fiitem_id' => $model->fiitem_id]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
    ?>
    <div class="form-group">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'fsname',

                'fsimage' => [
                    'attribute' => 'fsimage',
                    'class' => \yii\grid\DataColumn::className(),
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage, ['style' => 'max-width:200px']);
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'urlCreator'=>function($action, $model, $key, $index) use ($itemModel) {
                        return ['upload/' . $action,'id'=>$model->fiitem_pic_id, 'item' => $itemModel->fiitem_id, 'page' => (int)Yii::$app->request->get('page')];
                    },
                    'buttons' => [
                        'delete' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                ['delete', 'id' => $model->fiitem_pic_id],
                                [
                                    'title' => 'Удалить',
                                    'data-pjax' => '1',
                                    'data-method' => 'post',
                                ]
                            );
                        }
                    ]
                ],
            ],
        ]); ?>
        <?= Html::a('Добавить изображение', \yii\helpers\Url::to(['upload/index','type' => 'image', 'item' => $model->fiitem_id, 'page' => Yii::$app->request->get('page', Yii::$app->request->post('page'))])) ?>
    </div>
    <?php endif; ?>

    <?= $form->field($model, 'similarItemIds')->label('Похожие товары')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            $itemList,
            'fiitem_id',
            function ($model) {return $model->fsitem_name;}
        ),
        'options' => ['prompt' => '', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
