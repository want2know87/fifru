<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fiitem_id') ?>

    <?= $form->field($model, 'fsitem_name') ?>

    <?= $form->field($model, 'fsitem_old_desc') ?>

    <?= $form->field($model, 'fsitem_picture') ?>

    <?= $form->field($model, 'fsitem_small_desc') ?>

    <?php // echo $form->field($model, 'fsparams_array') ?>

    <?php // echo $form->field($model, 'fianother_customer') ?>

    <?php // echo $form->field($model, 'fsitem_customer') ?>

    <?php // echo $form->field($model, 'fitemplate_id') ?>

    <?php // echo $form->field($model, 'fiitem_catalog_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
