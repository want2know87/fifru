<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
?>
<div class="items-form">

    <?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="form-group">
        <?php echo Html::textInput('name', '', ['class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <?php echo Html::hiddenInput('type', Yii::$app->request->get('type')); ?>
        <?php echo Html::hiddenInput('item', Yii::$app->request->get('item')); ?>
        <?php echo Html::hiddenInput('page', Yii::$app->request->get('page')); ?>
        <?php echo Html::fileInput('file'); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>