<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */

$msg = '';

if(!$model->isNewRecord) {
    $msg = \app\models\Message::findOne(['id' => $model->id]);
    $msg = $msg->translation;
}

?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->hiddenInput(['value' => 'app'])->label(false) ?>

    <?= $form->field($model, 'message')->textInput() ?>

    <div class="form-group">
        <label>Текст</label>
    <?= Html::textarea('translate[ru]', $msg, ['rows' => 6, 'class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
