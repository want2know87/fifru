<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Управление текстами');
?>
<div class="source-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать текст'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php $msg = Yii::$app->request->get('SourceMessageSearch');

    $msg = isset($msg['message-translate']) ? $msg['message-translate'] : '';

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'id',*/
            /*'category',*/
            'message:ntext',

            'message-translate' => [
                'label' => 'Перевод',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\models\SourceMessage $model */
                    $text = '';
                    foreach($model->messages as $msg) {
                        $text .= /*$msg->language . ': ' .*/ $msg->translation . "\n";
                    }
                    return $text;
                },
                'filter' => Html::input('text', 'SourceMessageSearch[message-translate]', $msg, ['class' => 'form-control']),
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
