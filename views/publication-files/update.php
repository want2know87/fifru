<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PublicationFiles */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Publication Files',
]) . ' ' . $model->fipublic_file_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publication Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fipublic_file_id, 'url' => ['view', 'id' => $model->fipublic_file_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="publication-files-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
