<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PublicationFiles */

$this->title = Yii::t('app', 'Create Publication Files');
?>
<div class="publication-files-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
