<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Town */

?>
<div class="town-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
