<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Town */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Town'),
]) . ' ' . $model->fstown_name;
?>
<div class="town-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
