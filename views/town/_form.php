<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Region;
use app\models\Country;

$listCountry = [
    '' => Yii::t('app', 'app.list.empty'),
];

$listRegion = [
    '' => Yii::t('app', 'app.list.empty'),
];

$country = Country::find()->all();
/** @var Country $countr */
foreach($country as $countr) {
    $listCountry[$countr->ficountry_id] = $countr->fscountry_name;
}

/* @var $this yii\web\View */
/* @var $model app\models\Town */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="town-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fstown_name')->textInput(['maxlength' => 55]) ?>

    <?= $form->field($model, 'fstown_name_en')->textInput(['maxlength' => 55]) ?>

    <?= $form->field($model, 'ficountry_id')->dropDownList($listCountry) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'app.create') : Yii::t('app', 'app.update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
