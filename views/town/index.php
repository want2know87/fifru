<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Region;
use app\models\Country;

$countrys = Country::getAll();
/* @var $this yii\web\View */
/* @var $searchModel app\models\TownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.towns');
?>
<div class="town-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'app.create-town'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fstown_name',
            /*'firegion_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'firegion_id',
                'format' => 'html',
                'value' => function($model) use ($regions) {
                    return $regions[$model->firegion_id];
                }
            ],*/
            'ficountry_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'ficountry_id',
                'format' => 'html',
                'value' => function($model) use ($countrys) {
                    return $countrys[$model->ficountry_id];
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
