<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Publications */
/* @var $form yii\widgets\ActiveForm */

$types = [
    0 => 'Каталог',
    1 => 'Публикация',
];

/*$js = <<< JS
$(function() {
     $(".publications-form").on("change", "input:file[name='files[]']", function (){
        var em = $(this).clone();
        em.val('');
        em.attr('value', '');
        em.replaceWith(em.val('').clone(true));
        var newel = $('<div/>').addClass('form-group').append(em);
        newel.insertAfter($(this).closest('.form-group'));
     });
  });
JS;

$this->registerJs($js);*/

?>

<div class="publications-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'fsname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsdesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fitype')->dropDownList($types) ?>

    <?= $form->field($model, 'fsimage')->fileInput() ?>

    <?php if(strlen($model->fsimage)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage, ['style' => 'max-width:400px;']) ?>
        </div>
    <?php endif; ?>

<!--    <div class="form-group">
        <label>Приложенные файлы</label>
    </div>

    <div class="form-group new-file">
        <?php /*echo Html::fileInput('files[]') */?>
    </div>-->

    <?php

    $itemModel = $model;
    $query = \app\models\PublicationFiles::find()->where(['fipublic_id' => $model->fipublic_id]);
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $query,
    ]);

    ?>


  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

    <?php if(!$model->isNewRecord): ?>
    <div class="form-group">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'fsname' => [
                    'attribute' => 'fsname',
                    'format' => 'raw',
                    'value' => function (\app\models\PublicationFiles $pub) {
                        return $pub->fsname . ' (' . round(filesize((Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $pub->fsfile))/(1024), 2) . ' Kb' .
                            ' ' . Html::a('открыть в окне', Yii::$app->request->baseUrl . '/' . $pub->fsfile, ['target' => '_blank']) . ')';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'urlCreator'=>function($action, $model, $key, $index) use ($itemModel) {
                        return ['publication-files/' . $action,'id'=>$model->fipublic_file_id, 'publication' => $itemModel->fipublic_id];
                    },
                    'buttons' => [
                        'delete' => function($url, $model) use($itemModel) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                ['publication-files/delete', 'id' => $model->fipublic_file_id, 'publication' => $itemModel->fipublic_id],
                                [
                                    'title' => 'Удалить',
                                    'class' => 'remove-btn',
                                    'data-pjax' => '1',
                                    'data-confirm' => 'Удалить?',
                                    'data-method' => 'post',
                                ]
                            );
                        }
                    ]
                ]
            ],
        ]); ?>
        <?= Html::a('Добавить файл', \yii\helpers\Url::to(['publication-files/create', 'fipublic_id' => $model->fipublic_id, 'page' => Yii::$app->request->get('page', Yii::$app->request->post('page'))])) ?>
    </div>
    <?php endif; ?>

</div>
