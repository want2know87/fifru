<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Publications */

$this->title = Yii::t('app', 'Create Publications');
?>
<div class="publications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
