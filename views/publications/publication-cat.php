<?php

use app\models\Publications;
use app\models\PublicationFiles;
use yii\helpers\Html;

/** @var Publications $model  */

$files = PublicationFiles::find()->where(['fipublic_id' => $model->fipublic_id])->all();

?>

<div class="col-sm-6 col-xs-12 padding-none catalog-file">
    <div class="col-xs-2 col-sm-1 padding-none image-cat">
        <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/public_file_cat.png') ?>
    </div>
    <div class="col-xs-10 col-sm-11">
        <div class="col-xs-12 padding-none title-cat">
            <?php echo $model->fsname ?>
        </div>
        <!--<div class="col-xs-12 padding-none description">
            <?php /*echo $model->fsdesc */?>
        </div>-->
        <?php foreach($files as $file): ?>
            <?php /** @var PublicationFiles $file */ ?>
            <div class="col-xs-12 padding-none cat-download">
                <?php // echo \yii\helpers\Html::a($file->fsname . '<span class="gray"> (' . round(@filesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file->fsfile)/1024, 0) . ' Кб)</span>', Yii::$app->request->baseUrl . '/' . $file->fsfile, ['target' => '_blank']) ?>
                <?php echo \yii\helpers\Html::a($file->fsname . ' (' . round(@filesize(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $file->fsfile)/1024, 0) . ' Кб)', \yii\helpers\Url::to(['publication-files/get-item', 'id' => $file->fipublic_file_id]), ['target' => '_blank']) ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>