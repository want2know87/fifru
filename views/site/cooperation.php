<?php
use app\models\SiteTemplates;
use yii\bootstrap\Modal;

/** @var SiteTemplates $template */
$template = SiteTemplates::findOne(['fscode' => 'site/cooperation']);
$this->title = $template->fstitle;
$this->seoFields = $template->fstemplate_seo;
eval("?>" . $template->fstemplate_code . "<?php ");
return;
?>

<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = Yii::t('app', 'cooperation.title');

$js = <<< JS

$(function() {

    $('.select-type').on('click', '.coop-button', function() {
        $('.select-type span.active').removeClass('active');
        $(this).addClass('active');
        $('.form input[name=type]').val($(this).hasClass('diller') ? 'diller' : 'installer');
        $('.form p.form-title .active').removeClass('active');
        $('.form p.form-title').find('.' + $('.form input[name=type]').val()).addClass('active');
    });

    $('form').on('click', '.submit', function() {
        $.post($(location).attr('href'), $('form').serializeArray())
            .success(function() {
                alert('Запрос отправлен, ожидайте с вами свяжуться сотрудники компании');
                $('.modal').modal('hide');
            })
            .error(function() {
                alert('Проверьте введенные данные');
            });
        return false;
    });

});

JS;

$this->registerJs($js);

?>

<div class="distributors-index">
    <div class="padding-none col-xs-12 where-buy">
        <?php echo Yii::t('app', 'cooperation.title') ?>
    </div>
    <div class="col-xs-12 padding-none gray-buy">
        <?php echo Yii::t('app', 'cooperation.text') ?>
    </div>
    <br/>
    <div class="col-xs-12 padding-none cooperation center-block">
        <?php if($result): ?>
            <div class="col-xs-12 title" style="text-align: center;">
                <?php echo Yii::t('app', 'cooperation.success') ?>
            </div>
        <?php else: ?>
            <div class="col-xs-12 form">
                <div class="col-xs-12 title">
                    <?php echo Yii::t('app', 'cooperation.select.type') ?>
                </div>
                <div class="col-xs-12 select-type">
                    <div class="col-xs-6 padding-none"><span class="diller active coop-button"><?php echo Yii::t('app', 'cooperation.diller') ?></span></div>
                    <div class="col-xs-6 padding-none"><span class="installer coop-button"><?php echo Yii::t('app', 'cooperation.installer') ?></span></div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-10 note packet">
                    <br/>
                    <span class="diller active"><?php echo Yii::t('app', 'cooperation.diller.text') ?></span>
                    <span class="installer"><?php echo Yii::t('app', 'cooperation.installer.text') ?></span>
                </div>
                <div class="col-xs-1"></div>

                <?php

                Modal::begin ( [
                    'header' => '',
                    'toggleButton' => [
                        'tag' => 'button',
                        'class' => 'btn submit',
                        'label' => Yii::t('app', 'cooperation.ticket'),
                    ]
                ] );

                $form = ActiveForm::begin([
                    'options' => ['class' => 'form-inline']
                ]) ?>
                <input type="hidden" name="type" value="diller" />
                <p class="form-title">
                    <span class="diller active"><?php echo Yii::t('app', 'cooperation.form.diller') ?></span>
                    <span class="installer"><?php echo Yii::t('app', 'cooperation.form.installer') ?></span>
                </p><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.name') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.name') ?>" name="name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.country') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.country') ?>" name="country">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none"><?php echo Yii::t('app', 'cooperation.form.phone') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.phone') ?>" name="phone">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered"><?php echo Yii::t('app', 'cooperation.form.town') ?></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="<?php echo Yii::t('app', 'cooperation.form.town') ?>" name="town">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none centered">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="Email" name="mail">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label padding-none"><?php echo Yii::t('app', 'cooperation.form.comment') ?></label>
                    <div class="col-sm-8">
                        <textarea name="comment" placeholder="<?php echo Yii::t('app', 'cooperation.form.comment') ?>" class="form-control"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="center-block">

                        <button class="btn submit"><?php echo Yii::t('app', 'cooperation.ticket') ?></button>
                    </div>
                </div>

                <?php $form->end(); ?>


                <?php Modal::end();?>
            </div>
        <?php endif; ?>
    </div>
</div>