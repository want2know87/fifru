<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<div class="col-xs-12 padding-none about-title">
    <h2><?php echo Yii::t('app', 'about.title') ?></h2><br/>
    <div class="col-xs-12 padding-none general-text"><?php echo Yii::t('app', 'about.title-text') ?></div>
</div>
<div class="col-xs-12 padding-none another-text">
    <?php echo Yii::t('app', 'about.all-text') ?>
</div>
