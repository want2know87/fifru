<?php
use yii\helpers\Html;
use app\models\Sertificats;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use app\models\PublicationFiles;
use yii\widgets\ListView;
use app\models\Publications;

/* @var $this yii\web\View */
/* @var bool $result */

$this->title = Yii::t('app', 'down.title');

$js = <<< JS

$(function() {

});

JS;

$this->registerJs($js);

?>

<div class="distributors-index download">
    <div class="padding-none col-xs-12 where-buy">
        <?php echo Yii::t('app', 'down.title') ?>
    </div>
<?php

$query = Publications::find()->where(['fitype' => 1]);

$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 99999,
    ],
]);

$publications = ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '/publications/publication',
    'options' => ['class' => 'list-view'],
    'itemOptions' => [
        'class' => 'block-item',
    ],
    'summaryOptions' => [
        'style' => 'display:none;'
    ],
]);

$queryCat = Publications::find()->where(['fitype' => 0]);

$providerCat = new ActiveDataProvider([
    'query' => $queryCat,
    'pagination' => [
        'pageSize' => 99999,
    ],
]);

$publicationsCat = ListView::widget([
    'dataProvider' => $providerCat,
    'itemView' => '/publications/publication-cat',
    'options' => ['class' => 'list-view'],
    'itemOptions' => [
        'class' => 'block-item',
    ],
    'summaryOptions' => [
        'style' => 'display:none;'
    ],
]);

echo Tabs::widget ( [
    'items' => [
        [
            'label' => Yii::t('app','down.cat'),
            'content' => $publicationsCat,
            'active' => true
        ],
        [
            'label' => Yii::t('app','down.pub'),
            'content' => $publications
        ],
    ]
] );

?>
    </div>
</div>