<?php
use yii\helpers\Html;
use app\models\Sertificats;

$certs = Sertificats::find()->all();

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'cert.title');


$this->registerJsFile('@web/js/jquery.mousewheel-3.0.6.pack.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/source/jquery.fancybox.pack.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('@web/js/source/jquery.fancybox.css');

$js = <<< JS
$(function () {
    $(document).ready(function() {
        $(".fancybox").fancybox();

        $('.certificats').on('click', '.certificat .title', function() {
            $(this).closest('.certificat').find('a').trigger('click');
        });

    });
});
JS;

$this->registerJs($js);

?>

<div class="distributors-index">
    <div class="padding-none col-xs-12 where-buy">
        <?php echo Yii::t('app', 'cert.title') ?>
    </div>
    <div class="col-xs-12 padding-none gray-buy">
        <?php echo Yii::t('app', 'cert.text') ?>
    </div>
    <br/>
    <div class="col-xs-12 padding-none certificats">

        <?php foreach($certs as $cert): /** @var Sertificats $cert */?>
            <div class="certificat col-xs-4 padding-none">
                <div class="col-xs-4 padding-none image">
                    <?php if(!empty($cert->fssert_img)) echo
                        Html::a(Html::img(Yii::$app->request->baseUrl . '/' . $cert->fssert_img), Yii::$app->request->baseUrl . '/' . $cert->fssert_img,  ['class' => 'fancybox']);
                    ?>
                </div>
                <div class="col-xs-8 padding-none info">
                    <div class="title">
                        <?php echo $cert->fssert_name ?>
                    </div>
                    <div class="text" title="<?php echo htmlspecialchars($cert->fssert_text) ?>">
                        <?php echo $cert->fssert_text ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>

