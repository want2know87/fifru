<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Ой ошибочка';
?>
<div class="site-error">

    <div class="col-xs-12 padding-none">
        <div class="col-xs-6 padding-none">
            <h1>Упсссс! <?php echo @$exception->statusCode ?></h1>
            <?php if(404 == @$exception->statusCode): ?>
            <span class="gray">Не можем найти страницу которую вы<br/>
                запросили, не расстраивайтесь, а лучше:
            </span><br/>
            <?php else: ?>
                <span class="gray"><?php echo $exception->getMessage() ?></span><br/>
            <?php endif; ?>
            <span class="links">
                <?php echo Html::a('Почитайте о нашей компании', ['site/about']) ?><br/>
                <?php echo Html::a('Посмотрите каталог товаров', ['catalog/index']) ?><br/>
                <?php echo Html::a('Напишите нам', 'mailto:info@tde-fif.ru') ?><br/>
            </span>
        </div>
        <div class="col-xs-6 padding-none">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/404.png') ?>
        </div>
    </div>

</div>
