<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistributorsPoint */

$this->title = Yii::t('app', 'Update Distributors: ') . ' ' . $model->fsaddress;
?>
<div class="distributors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
