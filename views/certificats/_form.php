<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sertificats */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sertificats-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'fssert_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fssert_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fssert_img')->fileInput() ?>

    <?php if($model->fssert_img): ?>
    <div class="form-group">
        <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fssert_img, ['style' => 'max-width:400px;']) ?>
    </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
