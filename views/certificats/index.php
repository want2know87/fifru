<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SertificatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sertificats');
?>
<div class="sertificats-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sertificats'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fssert_name',
            'fssert_text:ntext',
            'fssert_img' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fssert_img',
                'format' => 'raw',
                'value' => function(\app\models\Sertificats $model) {
                    return $model->fssert_img ? Html::img(Yii::$app->request->baseUrl . '/' . $model->fssert_img, ['style' => 'max-width:200px']) : '';
                },
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
