<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sertificats */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sertificats',
]) . ' ' . $model->fisert_id;

?>
<div class="sertificats-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
