<?php

use yii\helpers\Html;
use app\models\Town;
use app\models\Country;
use yii\data\ActiveDataProvider;
use app\models\Distributors;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistributorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'app.internal.distribs');

$list = [
    '' => Yii::t('app', 'app.list.empty'),
];

/*$towns = $list + Town::getAll();*/
$towns = Town::find()->orderBy('fstown_name');
if(Yii::$app->request->get('country')) {
    $towns->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
}
$towns = $list + ArrayHelper::map($towns->all(), 'fitown_id', 'fstown_name');

$distribs = $list + ArrayHelper::map(Distributors::find()->groupBy('fsname')->orderBy('fsname')->all(), 'fidistr_id', 'fsname');

$country = $list + Country::getAll();

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=&libraries=places&language=ru');
/*$this->registerJsFile('https://maps.gstatic.com/maps-api-v3/api/js/20/1/intl/ru_ALL/main.js');
$this->registerJsFile('https://maps.gstatic.com/maps-api-v3/api/js/20/1/intl/ru_ALL/places.js');*/

$js = <<< JS
$(function() {

    $('.filters').on('change select', '#select-country', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%country-url%', {
              country : $(this).val(),
              town : $("#select-town").val(),
              distrib : $("#select-distrib").val()
            }, function(data) {

                var active = $('#select-town').val();
                $('#select-town').find('option[value!=""]').remove();
                for(town in data.towns) {
                    $('#select-town').append(
                        $('<option/>')
                            .val(town)
                            .text(data.towns[town])
                    );
                }
                $('#select-town').val(active);
                $('#select-town').trigger('change', {internal : true});

                active = $('#select-distrib').val();
                $('#select-distrib').find('option[value!=""]').remove();
                for(distrib in data.distribs) {
                    $('#select-distrib').append(
                        $('<option/>')
                            .val(distrib)
                            .text(data.distribs[distrib])
                    );
                }
                $('#select-distrib').val(active);
                $('#select-distrib').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', '#select-town', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%town-url%', {
              town : $(this).val(),
              country : $("#select-country").val(),
              distrib : $("#select-distrib").val()
            }, function(data) {

                var active = $('#select-country').val();
                $('#select-country').find('option[value!=""]').remove();
                for(country in data.countrys) {
                    $('#select-country').append(
                        $('<option/>')
                            .val(country)
                            .text(data.countrys[country])
                    );
                }
                $('#select-country').val(active);
                $('#select-country').trigger('change', {internal : true});

                active = $('#select-distrib').val();
                $('#select-distrib').find('option[value!=""]').remove();
                for(distrib in data.distribs) {
                    $('#select-distrib').append(
                        $('<option/>')
                            .val(distrib)
                            .text(data.distribs[distrib])
                    );
                }
                $('#select-distrib').val(active);
                $('#select-distrib').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', '#select-distrib', function(context, data) {
        if(typeof  data == 'undefined') {
            $.getJSON('%distrib-url%', {
              distrib : $(this).val(),
              country : $("#select-country").val(),
              town : $("#select-town").val()
            }, function(data) {

                var active = $('#select-country').val();
                $('#select-country').find('option[value!=""]').remove();
                for(country in data.countrys) {
                    $('#select-country').append(
                        $('<option/>')
                            .val(country)
                            .text(data.countrys[country])
                    );
                }
                $('#select-country').val(active);
                $('#select-country').trigger('change', {internal : true});

                active = $('#select-town').val();
                $('#select-town').find('option[value!=""]').remove();
                for(town in data.towns) {
                    $('#select-town').append(
                        $('<option/>')
                            .val(town)
                            .text(data.towns[town])
                    );
                }
                $('#select-town').val(active);
                $('#select-town').trigger('change', {internal : true});
            });
        }
    });

    $('.filters').on('change select', 'select', function(context, data) {
        if($(this).val() != '') {
            $(this).addClass('green');
        }
        else {
            $(this).removeClass('green');
        }
        if(typeof data == 'undefined') {
            var filters = {};
            $(this).parents('.filters').find('input,select').each(function() {
                filters[$(this).attr('name')] = $(this).val();
            });
            $.get('%url%', filters)
            .done(function(data) {
                $('.list-view').replaceWith($(data).find('.list-view'));
            });
        }
    });
    $('.filters select').each(function() {
        $(this).trigger('change', {internal : true});
        //$(this).trigger('click', {internal : true});
        $(this).trigger('select', {internal : true});
    });
});
JS;

$js = str_replace( [
        '%url%',
        '%country-url%',
        '%town-url%',
        '%distrib-url%',
    ], [
        Url::to(['distributors/index']),
        Url::to(['distributors/select-country']),
        Url::to(['distributors/select-town']),
        Url::to(['distributors/select-distrib']),
    ], $js);

$this->registerJs($js);

$js = <<< JS
    $(function() {
        var marker;

        function getLatLngFromString(ll) {
            var latlng = ll.split(',')
            return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
        }

        $('.distributors-index').on('click', '.distributors-view .point .marker.link-map', function() {
            if(marker) {
                marker.setMap(null);
            }
            marker = new google.maps.Marker({
                position: getLatLngFromString($(this).data('map')),
                map: window.myMap
            });
            window.myMap.setZoom(16);
            window.myMap.setCenter(getLatLngFromString($(this).data('map')));
            $('#show-map').modal('show');
            $('#show-map .modal-header h5').replaceWith($('<h5/>').text(
                $(this).closest('.distributors-view').find('.col-xs-12.title').text()
            ));
        });
    });
JS;

$this->registerJs($js);

/*$js = <<< JS
    $(function() {
        function getLatLngFromString(ll) {
            var latlng = ll.split(',');
            return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
        }
        $('.distributors-index').on('click', '.show-close span', function() {
            if($(this).parents('.distributors-view').find('.google-map-block').is(':hidden')) {
                $(this).parents('.distributors-view').find('.google-map-block').show();
                $(this).parents('.distributors-view').find('.show-close span:first').hide();
                $(this).parents('.distributors-view').find('.show-close span:last').show();
                var center = $(this).parents('.distributors-view').find('.google-map-block').data('map');
                var mapOptions = {"center" : getLatLngFromString(center),"zoom":16};
                var container =  $(this).parents('.distributors-view').find('.gmap-map-canvas')[0];
                container.style.width = '100%';
                container.style.height = '210px';
                var gmap0 = new google.maps.Map(container, mapOptions);
                var marker = new google.maps.Marker({
                    position: getLatLngFromString(center),
                    map: gmap0
                });
                $(window).on('rerender-map', function() {
                    var center = gmap0.getCenter();
                    google.maps.event.trigger(gmap0, 'resize');
                    gmap0.setCenter(center);
                });

                $(window).trigger('rerender-map');
            }
            else {
                $(this).parents('.distributors-view').find('.google-map-block').hide();
                $(this).parents('.distributors-view').find('.show-close span:first').show();
                $(this).parents('.distributors-view').find('.show-close span:last').hide();
            }
        });
    })
JS;

$this->registerJs($js);*/

?>

<?php
$modal = \yii\bootstrap\Modal::begin([
    'id' => 'show-map',
    'class' => 'google-map',
    'header' => '<h5></h5>',
]);
echo '
    <div class="container-fluid">
        <div class="row">';
$coord = new LatLng(['lat' => 0, 'lng' => 0 ]);
$map = new Map([
  'center' => $coord,
  'zoom' => 16,
]);
$map->width = '100%';
$map->height = '400';

$js = <<< JS
    window.myMap = %mapName%;
JS;

$js = str_replace('%mapName%', $map->getName(), $js);
$map->appendScript($js);

echo $map->display();
echo '
        </div>
    </div>';

$js = <<< JS
$(function () {
    $("#show-map").on('show.bs.modal', function() {
        window.setTimeout(function() {
            var center = window.myMap.getCenter();
            google.maps.event.trigger(window.myMap, "resize");
            window.myMap.setCenter(center);
        }, 500);
    });
});
JS;

$js = str_replace('%mapName%', $map->getName(), $js);
$this->registerJs($js);

$modal->end();
?>

<div class="distributors-index">

    <div class="col-xs-12 where-buy">
        <?php echo Yii::t('app', 'where.buy') ?>
    </div>

    <br/>
    <br/>

    <div class="col-xs-12 padding-none gray-buy">
        <?php echo Yii::t('app', 'where.buy-filters') ?>
    </div>

    <br/>
    <br/>

    <div class="col-xs-12 padding-none filters">
        <div class="col-xs-4 padding-none">
            <label for="select-country"><?php echo Yii::t('app', 'select.country') ?></label>
            <?php echo Html::dropDownList('country', 'null', $country, ['id' => 'select-country', 'class' => 'form-control']); ?>
        </div>
        <div class="col-xs-4">
            <label for="select-town"><?php echo Yii::t('app', 'select.town') ?></label>
            <?php echo Html::dropDownList('town', 'null', $towns, ['id' => 'select-town', 'class' => 'form-control']); ?>
        </div>
        <!--<div class="col-xs-3">
            <label for="select-region"><?php /*echo Yii::t('app', 'select.region') */?></label>
            <?php /*echo Html::dropDownList('region', 'null', $regions, ['id' => 'select-region', 'class' => 'form-control']); */?>
        </div>-->
        <div class="col-xs-4">
            <label for="select-distrib"><?php echo Yii::t('app', 'select.distrib') ?></label>
            <?php echo Html::dropDownList('distrib', (int)Yii::$app->request->get('distrib'), $distribs, ['id' => 'select-distrib', 'class' => 'form-control']); ?>
            <?php /*echo \yii\jui\AutoComplete::widget([
                'id' => 'select-distrib',
                'name' => 'distrib',
                'options' => [
                    'class' => 'form-control',
                ],
                'clientEvents' => [
                    'select' => 'function () { $("#select-country").trigger("change"); }',
                ],
                'clientOptions' => [
                    'minLength' => 3,
                    'source'=>new JsExpression("function(request, response) {
                        $.getJSON('". \yii\helpers\Url::to(['distributors/get-distrib']) ."', {
                            letters: $(\"#select-distrib\").val(),
                            country: $('#select-country').val(),
                            town: $('#select-town').val()
                        }, response);
                    }"),
                ],
            ]); */?>
        </div>
        <!--<div class="col-xs-4">
            <label for="select-distrib"><?php /*echo Yii::t('app', 'select.distrib') */?></label>
            <?php /*echo Html::textInput('distrib', '', ['id' => 'find-distrib', 'class' => 'form-control']); */?>
        </div>-->
    </div>

    <br/>
    <br/>
    <br/>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?php

 /*   $query = Distributors::find()->leftJoin('distributors_point', 'distributors_point.fidistr_id = distributors.fidistr_id');
    if(Yii::$app->request->get('country')) {
        $query->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
    }

    if(Yii::$app->request->get('town')) {
        $query->andWhere(['distributors_point.fitown_id' => (int)Yii::$app->request->get('town')]);
    }

    if(Yii::$app->request->get('distrib')) {
        $query->andWhere('fsname like :distr', ['distr' => '%' . Yii::$app->request->get('distrib') . '%']);
    }

    $query->orderBy('distributors_point.fitown_id');*/

    /*if(Yii::$app->request->get('region')) {
        $query->where(['firegion_id' => (int)Yii::$app->request->get('region')]);
    }*/

    $query = Town::find();
    if(Yii::$app->request->get('town')) {
        $query->andWhere(['fitown_id' => (int)Yii::$app->request->get('town')]);
    }
    if(Yii::$app->request->get('country')) {
        $query->andWhere(['ficountry_id' => (int)Yii::$app->request->get('country')]);
    }
    $query->orderBy('fstown_name');

    $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 99999,
        ],
    ]);

    ?>

    <div>
    <?php

    echo \yii\widgets\ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '/distributors/view-town',
        'options' => ['class' => 'list-view'],
        'itemOptions' => [
            'class' => 'block-item',
        ],
        'summaryOptions' => [
            'style' => 'display:none;'
        ],
    ]);

    /*echo \yii\widgets\ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '/distributors/view',
        'options' => ['class' => 'list-view'],
        'itemOptions' => [
            'class' => 'block-item',
        ],
        'summaryOptions' => [
            'style' => 'display:none;'
        ],
    ]);*/

    ?>

    </div>


    <?php /*echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fitown_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'fitown_id',
                'format' => 'html',
                'value' => function($model) use ($towns) {
                    return $towns[$model->fitown_id];
                }
            ],
            'ficountry_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'ficountry_id',
                'format' => 'html',
                'value' => function($model) use ($country) {
                    return $country[$model->ficountry_id];
                }
            ],
            'firegion_id' => [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'firegion_id',
                'format' => 'html',
                'value' => function($model) use ($regions) {
                    return $regions[$model->firegion_id];
                }
            ],
            'fsname',
            // 'fsaddress',
            // 'maps',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); */?>

</div>
