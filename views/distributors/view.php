<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use app\models\DistributorsPoint;
use yii\helpers\ArrayHelper;
use app\models\Town;

/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

$towns = ArrayHelper::map(Town::find()->all(),'fitown_id', 'fstown_name');

$points = DistributorsPoint::findAll(['fidistr_id' => $model->fidistr_id]);

?>
<div class="distributors-view">

    <div class="col-xs-12 padding-none">
        <div class="col-xs-12 padding-none fix-height">
            <div class="col-xs-12 title"><?php echo $model->fsname ?></div>
            <?php foreach($points as $modelPoint): ?>
                <?php
                    $mails = [];
                    $phones = [];
                    foreach($modelPoint->distributorsPointMails as $m) {
                        if(strlen($m->fsmail)) {
                            $mails[] = $m;
                        }
                    }
                    foreach($modelPoint->distributorsPointPhones as $p) {
                        if(strlen($p->fsphone)) {
                            $phones[] = $p;
                        }
                    }
                ?>
            <div class="col-xs-12 padding-none">
                <div class="col-xs-3 info marker"><?php echo $modelPoint->fsaddress ?></div>
                <div class="col-xs-3 <?php if(!empty($phones)) echo "info phone" ?>"><?php
                    foreach($phones as $phone) {
                        echo $phone->fsphone, "<br/>";
                    }
                    ?></div>
                <div class="col-xs-3 <?php if(strlen($modelPoint->fssite)) echo "info site" ?>"><a href="http://<?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?>" target="_blank"><?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?></a></div>
                <div class="col-xs-3 <?php if(!empty($mails)) echo "info mail" ?>"><?php
                    foreach($mails as $mail) {
                        echo '<a href="mailto:',htmlspecialchars($mail->fsmail),'">',$mail->fsmail, "</a><br/>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-xs-12">
                <!--<div class="col-xs-3 padding-none center-block show-close">
                    <?php /*if('' != trim($modelPoint->maps)): */?>
                        <span class="show-map"><?php /*echo Yii::t('app', 'show.map') */?></span>
                        <span class="show-map" style="display: none;"><?php /*echo Yii::t('app', 'hide.map') */?></span>
                    <?php /*endif; */?>
                    <br/>
                </div>-->
            </div>
            <?php
            if('' != trim($modelPoint->maps)) {
                $temp = $modelPoint->maps;
                $temp = explode(',', trim($temp, '()'));
                if(count($temp) == 2) {
                    $coord = new LatLng(['lat' => $temp[0], 'lng' => $temp[1]]);
                    $map = new Map([
                        'center' => $coord,
                        'zoom' => 16,
                    ]);
                    $map->width = '100%';
                    $map->height = '210px';
                    $js = <<< JS
                                    function getLatLngFromString(ll) {
                                        var latlng = ll.split(',')
                                        return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
                                    }
                                    marker = new google.maps.Marker({
                                    position: getLatLngFromString('%location%'),
                                    map: %mapName%
                                });
                                $(window).on('rerender-map', function() {
                                    var center = %mapName%.getCenter();
                                    google.maps.event.trigger(%mapName%, 'resize');
                                    %mapName%.setCenter(center);
                                });
JS;
                    $js = str_replace(array('%location%', '%mapName%'), array(trim($modelPoint->maps,'()'), $map->getName()), $js);
                    //$map->appendScript($js);
                    //echo $map->display();
                }
            }
            ?>
            <div class="col-xs-12 padding-none google-map-block" data-map="<?php echo trim($modelPoint->maps,'()') ?>" style="display: none;">
                <div class="gmap-map-canvas"></div>
                <?php
                if(isset($map)) {
                    //echo $map->display();
                }
                ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php /*echo DetailView::widget([
        'model' => $modelPoint,
        'attributes' => [
            'fidistr_id',
            'fitown_id',
            'ficountry_id',
            'firegion_id',
            'fsname',
            'fsaddress',
            'maps',
        ],
    ]) */?>

</div>
