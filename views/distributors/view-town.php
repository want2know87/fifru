<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 19.03.2015
 * Time: 23:47
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use app\models\DistributorsPoint;
use yii\helpers\ArrayHelper;
use app\models\Town;
use yii\bootstrap\BootstrapPluginAsset;
/* @var $this yii\web\View */
/* @var $model app\models\Town */

/** 2016-11-03 */
$this->registerJs(<<<SCRIPT
    $('#filter-apply').click(function (e) {
        e.preventDefault();
        if ($('#checkbox1').is(':checked')) {
            $('[data-wholesale="0"]').hide();
        } else {
            $('[data-wholesale="0"]').show();
        }
        if ($('#checkbox2').is(':checked')) {
            $('[data-retail="0"]').hide();
        } else {
            $('[data-retail="0"]').show();
        }
    });
    /*$('#checkbox1').change(function () {
        var wholesale = $('#checkbox1').is(':checked');
        var retail = $('#checkbox2').is(':checked') ? 1 : 0;
        if (wholesale) {
            $('[data-wholesale="0"]').hide();
            $('[data-wholesale="1"]'+'[data-retail='+retail+']').show();
        } else {
            $('[data-wholesale="0"]'+'[data-retail='+retail+']').show();
            $('[data-wholesale="1"]').hide();
        }
    });
    $('#checkbox2').change(function () {
        var retail = $('#checkbox2').is(':checked');
        var wholesale = $('#checkbox1').is(':checked') ? 1 : 0;
        if (retail) {
            $('[data-retail="0"]').hide();
            $('[data-retail="1"]'+'[data-wholesale='+wholesale+']').show();
        } else {
            $('[data-retail="0"]'+'[data-wholesale='+wholesale+']').show();
            $('[data-retail="1"]').hide();
        }
    });*/
SCRIPT
);
/** ********** */

$townName = $model->fstown_name;
$townId = $model->fitown_id;
$distributors = \app\models\Distributors::find()
    ->where(['distributors_point.fitown_id' => $model->fitown_id])
    ->innerJoin('distributors_point', 'distributors.fidistr_id = distributors_point.fidistr_id')
;

$distributors
    ->andFilterWhere([
        'distributors_point.distribution_type_wholesale' => Yii::$app->request->get('wholesale'),
        'distributors_point.distribution_type_retail' => Yii::$app->request->get('retail'),
    ]);

if(Yii::$app->request->get('distrib')) {
    $distributors->andWhere('distributors_point.fidistr_id = :distr', ['distr' => (int)Yii::$app->request->get('distrib')]);
}
// $distributors->orderBy('fsname');
$distributors->orderBy([
    'priority' => SORT_DESC,
    'fsname' => SORT_ASC,
]);
$distributors = $distributors->all();

if(!Yii::$app->request->isAjax) {
    $distributors = [];
}

if(!empty($distributors)) {
    echo '<div class="col-xs-12 town-name padding-none">', $townName, '</div><p class="clearfix">&nbsp;</p>';
}

$this->registerJs("
    $(function() {
        $('body').on('submit','.btncallback', function() {
            var mails=[]
            \$(this).parent().parent().find('.mailto').each(function( index ) {
                mails.push(\$(this).text());
            });
            \$.post('',{
                '_csrf':\$('meta[name=\"csrf-token\"]').attr('content'),
                'tel':\$(this).find('input[type=\"tel\"]').val(),
                'mail':mails
            },function(data){
                if (data.send=='yes'){
                    \$('input[type=\"tel\"]').val('')
                    alert('Ваше сообщение отправлено!')
                }
            },'json');

            return false;
        })
    });
");


?>

<div class="examle-wrap distributors-view">
  <div class="example">
    <div class="panel-group panel-group-continuous" id="exampleAccordionContinuous"
    aria-multiselectable="true" role="tablist">

    <?php foreach($distributors as $distrib) {

        $model = $distrib;
        $points = DistributorsPoint::find()->with('distributorsPointMails','distributorsPointPhones')->where(['fidistr_id' => $model->fidistr_id, 'fitown_id' => $townId])->all();
        if(empty($points)) {
            continue;
        }

        ?>

        <?php foreach($points as $modelPoint): ?>
            <?php
            $mails = [];
            $phones = [];
            foreach($modelPoint->distributorsPointMails as $m) {
                if(strlen($m->fsmail)) {
                    $mails[] = $m;
                }
            }
            foreach($modelPoint->distributorsPointPhones as $p) {
                if(strlen($p->fsphone)) {
                    $phones[] = $p;
                }
            }

            ?>

          <div class="panel"
        data-wholesale="<?= $modelPoint->distribution_type_wholesale ?>"
        data-retail="<?= $modelPoint->distribution_type_retail ?>"
          >
            <div onclick="disclick<?=$modelPoint->fidistr_point_id?>();" class="panel-heading title collapsed" id="exampleHeadingContinuousThree" role="tab" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
              href="#exampleCollapseContinuous<?= $modelPoint->fidistr_point_id ?>" aria-controls="exampleCollapseContinuous<?= $modelPoint->fidistr_point_id ?>"
              aria-expanded="false">
              <div class="col-sm-5 col-xs-11 name"><?php echo $model->fsname ?></div>
              <div class="col-sm-6 hidden-xs adres"><?php echo $modelPoint->fsaddress ?></div>
            </div>
            <div class="panel-collapse collapse hidden-xs" id="exampleCollapseContinuous<?= $modelPoint->fidistr_point_id ?>" aria-labelledby="exampleHeadingContinuousThree"
            role="tabpanel">
              <div class="panel-body padding-none fix-height">
                <div class="row">
                    <div class="hidden-xs col-md-8 <?= (strlen(trim($modelPoint->maps))) ? '':'hidden'?>">
                        <div id="map<?=$modelPoint->fidistr_point_id?>" style="height:245px;<?php /*echo 'width:100%;height:auto;';*/ ?>"></div>

                        <script type="text/javascript">

                                function disclick<?=$modelPoint->fidistr_point_id?>() {
                                    function initMap<?=$modelPoint->fidistr_point_id?>() {
                                      var myLatLng<?=$modelPoint->fidistr_point_id?> = new google.maps.LatLng(<?php echo trim($modelPoint->maps,'()')?>);

                                      var map<?=$modelPoint->fidistr_point_id?> = new google.maps.Map(document.getElementById('map<?=$modelPoint->fidistr_point_id?>'), {
                                        zoom: 16,
                                        center: myLatLng<?=$modelPoint->fidistr_point_id?>
                                      });
                                      window.myMap<?=$modelPoint->fidistr_point_id?> = map<?=$modelPoint->fidistr_point_id?>;

                                      var marker = new google.maps.Marker({
                                        position: myLatLng<?=$modelPoint->fidistr_point_id?>,
                                        map: map<?=$modelPoint->fidistr_point_id?>
                                      });
                                    }
                                    initMap<?=$modelPoint->fidistr_point_id?>();


                                    window.setTimeout(function() {
                                        var center = window.myMap<?=$modelPoint->fidistr_point_id?>.getCenter();
                                        google.maps.event.trigger(window.myMap<?=$modelPoint->fidistr_point_id?>, 'resize');
                                        window.myMap<?=$modelPoint->fidistr_point_id?>.setCenter(center);
                                    }, 500);
                                };

                        </script>


                    </div>
                    <div class="col-md-4 <?= (strlen(trim($modelPoint->maps))) ? '':'col-md-offset-4'?>">

                        <div class="col-xs-12 col-sm-6 col-md-12 padding-none point">

                            <div class=" col-xs-12 info marker <?php if(strlen(trim($modelPoint->maps))) echo "link-map1" ?>" data-map="<?php echo trim($modelPoint->maps,'()')?>"><?php echo $modelPoint->fsaddress ?></div>
                            <div class="col-xs-12 <?php if(!empty($phones)) echo "info phone" ?>"><?php
                                foreach($phones as $phone) {
                                    echo $phone->fsphone, "<br/>";
                                }
                                ?></div>
                            <div class=" col-xs-12 <?php if(strlen($modelPoint->fssite)) echo "info site" ?>"><a href="http://<?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?>" target="_blank"><?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?></a></div>
                            <div class=" col-xs-12 <?php if(!empty($mails)) echo "info mail" ?>"><?php
                                foreach($mails as $mail) {
                                    echo '<a class="mailto" href="mailto:',htmlspecialchars($mail->fsmail),'">',$mail->fsmail, "</a><br/>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12 padding-none input-group <?=(empty($mails)) ? 'hidden':''?>">
                            <p><b>Заказ обратного звонка:</b></p>
                            <form action="" method="post" class="btncallback">
                              <input type="tel" class="form-control" placeholder="Введите ваш номер телефона" required>
                              <input type="submit" class="btn" value="Заказать">
                            </form>
                        </div>

                    </div>
                </div>

              </div>
            </div>

            <div class=" visible-xs" >
              <div class="panel-body padding-none fix-height">
                <div class="row">
                    <div class="col-md-4 <?= (strlen(trim($modelPoint->maps))) ? '':'col-md-offset-4'?>">

                        <div class="col-xs-12 col-sm-6 col-md-12 padding-none point">

                            <div class=" col-xs-12 info marker <?php if(strlen(trim($modelPoint->maps))) echo "link-map" ?>" data-map="<?php echo trim($modelPoint->maps,'()')?>"><?php echo $modelPoint->fsaddress ?></div>
                            <div class="col-xs-12 <?php if(!empty($phones)) echo "info phone" ?>"><?php
                                foreach($phones as $phone) {
                                    echo $phone->fsphone, "<br/>";
                                }
                                ?></div>
                            <div class=" col-xs-12 <?php if(strlen($modelPoint->fssite)) echo "info site" ?>"><a href="http://<?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?>" target="_blank"><?php echo str_replace('http://','',htmlspecialchars($modelPoint->fssite)) ?></a></div>
                            <div class=" col-xs-12 <?php if(!empty($mails)) echo "info mail" ?>"><?php
                                foreach($mails as $mail) {
                                    echo '<a class="mailto" href="mailto:',htmlspecialchars($mail->fsmail),'">',$mail->fsmail, "</a><br/>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12 padding-none input-group <?=(empty($mails)) ? 'hidden':''?>">
                            <p><b>Заказ обратного звонка:</b></p>
                            <form action="" method="post" class="btncallback">
                              <input type="tel" class="form-control" placeholder="Введите ваш номер телефона" required>
                              <input type="submit" class="btn" placeholder="Заказать">
                            </form>
                        </div>

                    </div>
                </div>

              </div>
            </div>

          </div>


        <?php endforeach; ?>


    <?php } ?>

    </div>
  </div>
</div>
