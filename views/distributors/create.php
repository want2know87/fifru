<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distributors */

$this->title = Yii::t('app', 'app.internal.create-distrib');

?>
<div class="distributors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
