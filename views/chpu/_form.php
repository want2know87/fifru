<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Chpu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chpu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fsyii_link')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fsurl')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
