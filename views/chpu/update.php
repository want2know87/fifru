<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Chpu */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'ЧПУ ',
]) . ' ' . $model->fsurl;
?>
<div class="chpu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
