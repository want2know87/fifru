<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CatalogRu;

// echo $this->render('//site/language.php', ['config' => $model->translates, 'class' => $model::className()]);

$parent = [
    '' => Yii::t('app', "app.list.empty"),
];
$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => null])->orWhere(['fiparent_catalog_id'=>''])->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => array_filter(array_keys($parent))])->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => array_filter(array_keys($parent))])->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $parent[$parents->ficatalog_id] = $parents->fsname;
}

/* @var $this yii\web\View */
/* @var $model app\models\CatalogRu */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/js/ckeditor/ckeditor.js');
$this->registerJs("
        CKEDITOR.replace('catalogru-fscatalog_text',
            {
                height  : '400px',
                on: {
                instanceReady:function( ev ){
                   jQuery(CKEDITOR.instances['catalogru-fscatalog_text'].container.$).mouseleave(function() {
                      CKEDITOR.instances['catalogru-fscatalog_text'].updateElement();
                   });
                   CKEDITOR.instances['catalogru-fscatalog_text'].on('blur', function() {
                      CKEDITOR.instances['catalogru-fscatalog_text'].updateElement();
                   });
                }
             }
            });
        $('#catalogru-fscatalog_text').on('change', function() {
          CKEDITOR.instances['catalogru-fscatalog_text'].setData($(this).val());
        });
        /*CKEDITOR.replace('catalogru-fscatalog_text',
            {
                height  : '400px',
                on: {
                instanceReady:function( ev ){
                   jQuery(CKEDITOR.instances['catalogru-fscatalog_text'].container.$).mouseleave(function() {
                      CKEDITOR.instances['catalogru-fscatalog_text'].updateElement();
                   });
                   CKEDITOR.instances['catalogru-fscatalog_text'].on('blur', function() {
                      CKEDITOR.instances['catalogru-fscatalog_text'].updateElement();
                   });
                }
             }
            });*/
            ");

$options = [
    0 => 'Нет',
    1 => 'Да',
];

?>

<?php yii\web\JqueryAsset::register($this); ?>

<div class="catalog-ru-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'fsname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fivisible')->dropDownList($options) ?>

    <?= $form->field($model, 'fiparent_catalog_id')->dropDownList($parent) ?>

    <?= $form->field($model, 'fscatalog_img')->fileInput() ?>
    <?php if(strlen($model->fscatalog_img)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fscatalog_img, ['style' => 'max-width:200px;max-height:200px;']); ?>
        </div>
    <?php endif; ?>
    <?php /*= $form->field($model, 'fscatalog_img')->textInput(['maxlength' => 255]) */?>

    <?= $form->field($model, 'fscatalog_text')->textarea() ?>
    
    <?= $form->field($model, 'fscatalog_bottom_text')->textarea() ?>

    <?= $form->field($model, 'fishow_general_desc')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

    <?= $form->field($model, 'fishow_with_items')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

    <?= $form->field($model, 'fsdestination')->textarea() ?>

    <?= $form->field($model, 'fswork')->textarea() ?>

    <?= $form->field($model, 'fsuse')->textarea() ?>

    <?= $form->field($model, 'fscatalog_additional_test')->textarea() ?>

    <?= $form->field($model, 'fsseo')->textarea() ?>

    <?= $form->field($model, 'fsicon')->fileInput() ?>
    <?php if(strlen($model->fsicon)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsicon, ['style' => 'max-width:200px;max-height:200px;']); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'fsbigicon')->fileInput() ?>
    <?php if(strlen($model->fsbigicon)): ?>
        <div class="form-group">
            <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsbigicon, ['style' => 'max-width:300px;max-height:300px;', 'class' => 'img-thumbnail']); ?>
        </div>
    <?php endif; ?>

  <?php if(!$model->isNewRecord): ?>
    <?php

    $catsIds = CatalogRu::find()->where(['fiparent_catalog_id' => (int)$model->ficatalog_id])->all();
    $catsIds = \yii\helpers\ArrayHelper::map($catsIds, 'ficatalog_id', 'fsname');
    $catsIds = array_merge(
      [(int)$model->ficatalog_id],
      array_keys($catsIds)
    );

    $params = Yii::$app->db->createCommand(
      'SELECT DISTINCT
        pc.fiparam_id as `id`, pc.fsparam_name AS `name`
      FROM
        params_values pv
      LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
      WHERE
        pv.fiitem_id IN(
          SELECT
            fiitem_id
          FROM
            catalog_items
          WHERE
            ficatalog_id IN (' . implode(',', $catsIds) . ')
        )
      ORDER BY
        (
          CASE
          WHEN pv.fiorder IS NULL THEN
            99999
          ELSE
            pv.fiorder
          END
        )')
      ->queryAll(PDO::FETCH_ASSOC);

    $params = \yii\helpers\ArrayHelper::map($params, 'id', 'name');

    $activeFilters = $model->filters;
    if(!empty($activeFilters)) {
      $activeFilters = json_decode($activeFilters, true);
    }

    ?>
    <div class="form-group">
      <label>Фильтры для каталога</label>
        <?php echo Html::checkboxList('active_filters', $activeFilters, $params, ['separator' => '<br/>']); ?><br/>
    </div>
  <?php endif; ?>

    <?= $form->field($model, 'fslink')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'fstemplate_code')->textInput(['maxlength' => 32]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
