<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\typeahead\Typeahead;

$data = isset($data) ? $data : [];
?>
<div class="col-xs-12 padding-none filter" style="margin-bottom: 10px;">
    <style>
        .form-control.tt-input{ 
            background: #fff!important;; 
            border: 1px solid #CCCCCC!important;
        }
    </style>
<?php Pjax::begin(['enablePushState' => false]); ?>
    <h4>Поиск по аналогам</h4>
    <p>Корректно введите в строке поиска название аналога</p>
    <h5><?=Yii::$app->request->post('q')?></h5>
    <?=Html::beginForm(['test/item'], 'post', ['data-pjax' => '', 'class' => 'form-inline'])?>
        <?php /*echo Html::textInput('q', Yii::$app->request->post('q'), []);*/ ?>
        <div class="form-group">
        <?=Typeahead::widget([
            'name' => 'q',
            'dataset' => [
                [
                    'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                    'display' => 'value',
                    'remote' => [
                        'url' => Url::to(['test/suggest']) . '?q=%QUERY',
                        'wildcard' => '%QUERY'
                    ],
                    'limit' => 10
                ]
            ],
            'options' => ['class' => 'form-control'],
            'pluginOptions' => ['highlight'=>true],
        ])?>
        </div>
        <?=Html::submitButton('Поиск', ['class' => 'btn btn-default', 'style' => 'margin-top: 3px;'])?>
    <?=Html::endForm()?>
    <ul>
        <?php foreach ($data as $item): ?>
        <?php $url = str_replace('fif.by', 'tde-fif.ru', $item['url']) ?>
        <li><a href="<?=$url?>"><?=$item['fsitem_name']?></a></li>
        <?php endforeach;?>
    </ul>
<?php Pjax::end(); ?>
</div>
