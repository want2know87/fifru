<?php

use yii\helpers\Html;
use app\models\CatalogRu;

$parent = [];
$parentsQeury = CatalogRu::find()->where(['fiparent_catalog_id' => null])->andWhere(['fivisible' => 1])->orWhere(['fiparent_catalog_id'=>''])->orderBy('fiorder')->all();
/** @var CatalogRu $parents */
foreach($parentsQeury as $parents) {
    $translates = json_decode($parents->translates, true);
    $parent[] = [
        'id' => $parents->ficatalog_id,
        'name' => $translates['values'][Yii::$app->language]['fsname'],
        'big-image' => $parents->fsbigicon,
        'link' => $parents->fslink,
        'subcat' => [],
    ];
}

$all = CatalogRu::find()->where(['fivisible' => 1])->orderBy('fiorder')->all();
/** @var CatalogRU $row */
foreach($all as $row) {
    foreach($parent as $index => $category) {
        if($category['id'] == $row->fiparent_catalog_id) {
            $translates = json_decode($row->translates, true);
            $parent[$index]['subcat'][] = [
                'id' => $row->ficatalog_id,
                'name' => $translates['values'][Yii::$app->language]['fsname'],
                'subcat' => [],
                'icon' => $row->fsicon];
        }
    }
}

foreach($all as $row) {
    foreach($parent as $indcat => $category) {
        foreach($category['subcat'] as $index => $subcat) {
            if($subcat['id'] == $row->fiparent_catalog_id) {
                $translates = json_decode($row->translates, true);
                $parent[$indcat]['subcat'][$index]['subcat'][] = [
                    'id' => $row->ficatalog_id,
                    'name' => $translates['values'][Yii::$app->language]['fsname'],
                    'subcat' => []];
            }
        }
    }
}

$activeCat = Yii::$app->request->get('cat');
if(empty($activeCat)) {
    $activeCat = $parent[0]['id'];
}

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = Yii::t('app', 'app.cat');*/

//print_r($parent);

?>
<div class="catalog-general">
    <div class="col-xs-12 padding-none info-block">
        <div class="col-xs-4 padding-none">
            <span class="title"><?php echo Yii::t('app', 'catalog.title') ?></span>
        </div>
        <div class="col-xs-4 padding-none">
            <?php

            $filename = '';
            $scan = scandir(__DIR__ . '/../../web', SCANDIR_SORT_DESCENDING);
            foreach($scan as $file) {
                if(strpos($file, 'catalog_') === 0) {
                    $filename = $file;
                    break;
                }
            }

            ?>
            <div class="border">
                <div class="file">
                    <a href="<?php echo Yii::$app->request->baseUrl . '/' . $filename ?>">
                        <?php echo Html::img(Yii::$app->request->baseUrl . '/image/site/file.png') ?>
                    </a>
                </div>
                <div class="file-info">
                    <a href="<?php echo Yii::$app->request->baseUrl . '/' . $filename ?>"><span><?php echo Yii::t('app', 'catalog.all.production') ?></span></a><br/>
                    <span class="gray">(<?php echo strlen($filename) ? 'pdf, ' . round(filesize(__DIR__ . '/../../web/' . $filename)/1024/1024, 0) . ' ' . Yii::t('app', 'catalog.mb') : 'pdf, 27 мб' ?>)</span>
                </div>
            </div>
        </div>
        <div class="col-xs-4 padding-none">
            <div class="border last external">
                <span class="dashed-link"><a href=""><?php echo Yii::t('app', 'catalog.functional') ?></a></span><br/>
                <span class="dashed-link"><a href=""><?php echo Yii::t('app', 'catalog.analog') ?></a></span>
            </div>
        </div>
    </div>
    <?php echo $this->render('catalog-menu', ['catalogs' => $parent, 'active' => $activeCat]); ?>
    <div class="col-xs-12 padding-none catalogs-with-sub">
        <div class="col-xs-12 padding-none">
            <?php $i = 0; foreach($parent[0]['subcat'] as $category): ?>
                <?php if($i%4 == 0 && $i!=0) echo '</div><div class="col-xs-12 padding-none">'; ?>
                <?php $i++ ?>
                <div class="block-div col-xs-3 padding-none">
                    <div class="title-div"><?php
                        if(!empty($category['icon'])) echo "<div class=\"icon\"><img src=\"", Yii::$app->request->baseUrl,"/",$category['icon'],"\"/></div>"; ?>
                        <?php echo Html::a($category['name'], \yii\helpers\Url::to(['catalog/index','cat' => $category['id']]), ['class' => 'link ' . (empty($category['icon']) ? '' : 'big-padding-left')]) ?></div>
                    <?php if(empty($category['subcat'])): ?>
                        <div class="child-subcat link">
                            <?php echo Html::a(Yii::t('app', 'catalog.incat'), ['catalog/index','cat' => $category['id']]) ?>
                        </div>
                    <?php endif; ?>
                    <?php foreach($category['subcat'] as $sub): ?>
                        <div class="child-subcat link">
                            <?php echo Html::a($sub['name'], ['catalog/index','cat' => $sub['id']]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>