<?php
use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;

/** var array $item */

?>
<div class="col-xs-12 item-new-one padding-10 <?php if($item['fisale_id']) echo "sale"?> <?php if($item['finew']) echo "finew"?>" data-id="<?php echo $item['fiitem_id'] ?>">

    <?php
        $itemValues = @$itemsValues[$item['fiitem_id']];
        if(empty($itemValues)) {
            $itemValues = [];
        }
        $itemValues = array_map(
          function($el) {
            return @array_values($el)[0];
        }, $itemValues);
    ?>

    <div class="hidden-info" style="display: none;"><?php echo json_encode($itemValues) ?></div>
    <div class="col-xs-12 padding-none">
        <div class="img col-xs-3 padding-none">
            <?php $image = file_exists(Yii::$app->basePath . '/web/' . $item['fsitem_picture'] . '.small.jpg') ?
                Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'] . '.small.jpg' :
                Yii::$app->request->baseUrl . '/' . $item['fsitem_picture'];
            ?>
            <?php echo Html::a(
              Html::img($image, ['width'=>'100%', 'alt'=>$breadCrumbs['low_name'].' '.$item['fsitem_name']]),
              \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']])
            ) ?>
        </div>
        <div class="desc col-xs-9 padding-none" data-toggle="tooltip" data-placement="top" title="<?php echo $item['fsitem_small_desc'] ?>">
            <div class="description">
                <p class="title"><?php echo Html::a($item['fsitem_name'], \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']])) ?></p>
                <?php echo $item['fsitem_small_desc'] ?>
                    <?php /*if(!empty($itemsValues[$item['fiitem_id']])) {
                        $index = 0;
                        foreach(@$itemsValues[$item['fiitem_id']] as $id => $params) {
                            echo '<div class="col-xs-9 param-name padding-none"><div class="dots"></div><div class="name">',array_keys($params)[0], '</div></div><div class="col-xs-3 param-value padding-none">', array_values($params)[0], '</div>';
                            $index++;
                            if($index>=5) {
                                break;
                            }
                        }
                    }*/
                ?>
            </div>
        </div>
    </div>
    <div class="price col-xs-12 padding-none">
        <div class="col-sm-3 col-xs-6 padding-none price-cell">
            <?php
            $itemPrice = [];
            foreach($prices as $name) {
                if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
                    $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
                }
            }
            if(!empty($itemPrice)) {
                echo '<span class="price-name">Цена: </span>';
                $priceNameDef = '';
                foreach($itemPrice as $name => $pr) {
                    echo '<span class="number">',$pr,'</span>';
                    $priceNameDef = $name;
                    break;
                }
                $items = [];
                foreach($itemPrice as $name => $pr) {
                    $items[] = [
                        'label' => $name,
                        'options' => [
                            'data-price' => $pr,
                        ],
                    ];
                }
                echo ButtonDropdown::widget ( [
                    'label' => $priceNameDef,
                    'options' => [
                        'class' => 'btn-lg btn-default' . (count($itemPrice) > 1 ? '' : ' disabled' ),
                    ],
                    'dropdown' => [
                        'items' => $items,
                    ],
                ] );

            }
            ?>
        </div>
        <div class="col-sm-3 col-md-2 col-xs-6 padding-none wherebuy"><a href="<?php echo \yii\helpers\Url::to(['distributors/index']) ?>"><span class="where">Где купить</span></a></div>
        <div class="col-sm-3 col-md-4 hidden-xs padding-none btn-details">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/items', 'id' => $item['fiitem_id']]) ?>">
                <button class="btn btn-default" type="button">Подробнее о модели</button>
            </a>
        </div>
        <?php if($item['fisale_id']): ?>
        <div class="col-sm-3 sale-block hidden-xs">
            <span class="green-sale"><?php echo $item['fssale_name'] ?></span>
        </div>
        <?php endif; ?>
    </div>
</div>
