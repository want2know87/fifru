<?php

use app\models\Items;
use app\models\ItemsPictures;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
/** @var Items $item */

/*foreach($params as $param) {
    echo $param['name'], $param['value'];
}*/
$breadCrumbs['sublow_name']=$breadCrumbs['low_name'];
$breadCrumbs['sublow_id']=$breadCrumbs['low_id'];
$breadCrumbs['low_name']=$item->fsitem_name;
$i = 0;

$prices = ['BYR', 'RUR', 'USD'];

$pictures = ItemsPictures::find()->where(['fiitem_id' => $item->fiitem_id])->all();

$this->title = $item->fsitem_name;
$this->seoFields = $item->fsseo;

$properties = (new \yii\db\Query())
  ->select('pc.fsparam_name as `name`, pv.fsparam_value as `value`, pv.fiorder')
  ->from(['pv' => 'params_values'])
  ->innerJoin(['pc' => 'params_cat'], 'pv.fiparam_id = pc.fiparam_id')
  ->where(['pv.fiitem_id' => $item->fiitem_id])
  /*->orderBy(['CASE WHEN pv.fiorder IS NULL then 99999 ELSE pv.fiorder END'])*/
  ->all(Yii::$app->db);

usort($properties, function($el1, $el2) {
  return (int)$el1['fiorder'] > (int)$el2['fiorder'];
});

$tech = '';
$i = 0;
  foreach($properties as $prop) {
    $tech .= '<div class="col-xs-12 padding-none ' . ($i%2 == 0 ? 'gray' : '') . ' row">';
    $tech .= '  <div class="col-xs-8 padding-none">' . $prop['name'] . '</div>' . '<div class="col-xs-4 padding-none value">' . $prop['value'] . '</div>';
    $tech .= '</div>';
    $i++;
  }

$pictures = ItemsPictures::find()->where(['fiitem_id' => $item->fiitem_id])->all();

$scheme = '<div class="col-xs-12 padding-none schemas">';

$index = 0;

foreach($pictures as $picture) {
  $scheme .= '<div class="col-sm-6">';
  $scheme .= '<div class="col-xs-12 picture-image">' . \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $picture->fsimage, ['style' => 'max-width:330px;padding-top:10px;']) . '</div>';
  $scheme .= '<div class="col-xs-12 picture-title">' . $picture->fsname . '</div>';
  $scheme .= '</div>';
  if($index%2 === 1) {
    $scheme .= '<div class="col-xs-12"></div>';
  }
  $index++;
}

$scheme .= '</div>';

?>
<div class="col-xs-12 padding-none view-one">

<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs, 'social' => ['title' => $item->fsitem_name, 'link' => \yii\helpers\Url::to(['catalog/items', 'id' => 2], true)] ]) ?>

<div class="col-xs-12 padding-none all-div">
  <div class="col-sm-3 padding-none image">
    <h2 class="visible-xs col-xs-12 padding-none title"><?php echo Html::encode($item->fsitem_name) ?></h2>
    <?php if($item->fisale_id): ?>
      <?php
        $sale = \app\models\Sales::findOne(['fisale_id' => (int)$item->fisale_id]);
        if(!empty($sale)):?>
          <div class="col-xs-12 padding-none sales-text"><span><?php echo Html::encode($sale->fssale_name); ?></span></div>
      <?php endif; ?>
    <?php endif; ?>
    <div class="col-sm-12 col-xs-6 col-xs-offset-3 padding-none image">
      <?php echo \yii\helpers\Html::img(Yii::$app->request->baseUrl . '/' . $item->fsitem_picture,['alt'=>$breadCrumbs['sublow_name'].' '.$item->fsitem_name]) ?>
    </div>
    <div class="price col-xs-12 padding-none">
      <div class="col-xs-12 padding-none price-cell">
        <?php
        $itemPrice = [];
        foreach($prices as $name) {
          if(!empty($item['fsprice_' . strtolower($name)]) && strlen($item['fsprice_' . strtolower($name)])) {
            $itemPrice[$name] = $item['fsprice_' . strtolower($name)];
          }
        }
        if(!empty($itemPrice)) {
          echo '<span class="price-name">Цена</span>';
          $priceNameDef = '';
          foreach($itemPrice as $name => $pr) {
            echo '<span class="number">',$pr,'</span>';
            $priceNameDef = $name;
            break;
          }
          $items = [];
          foreach($itemPrice as $name => $pr) {
            $items[] = [
              'label' => $name,
              'options' => [
                'data-price' => $pr,
              ],
            ];
          }
          echo ButtonDropdown::widget ( [
            'label' => $priceNameDef,
            'options' => [
              'class' => 'btn-lg btn-default',
            ],
            'dropdown' => [
              'items' => $items,
            ],
          ] );

        }
        ?>
      </div>
    </div>
    <div class="col-xs-12 padding-none buy">
      <a href="<?php echo \yii\helpers\Url::to(['distributors/index']); ?>">
      <span class="buy">Где купить</span>
      </a>
    </div>
    <div class="clearfix"></div>
    <p>&nbsp;</p>
  </div>
  <div class="col-sm-9 padding-none props">
    <h1 class="hidden-xs col-xs-12 padding-none title"><?php echo Html::encode($item->fsitem_name) ?></h1>
    <?php if(strlen($item->fspassport && file_exists(Yii::getAlias('@webroot').'/'.$item->fspassport))):
    $divpassport='<div class="col-xs-12 padding-none div-passport description">'.
      '<div class="col-xs-5 col-sm-3 col-md-3 item-passport" style="text-align: left;">'.
            \yii\helpers\Html::a('Руководство на реле', Yii::$app->request->baseUrl . '/' . $item->fspassport,['target' => '_blank']).
      '</div>'.
      '<div class="col-xs-7 col-sm-9 col-md-9">'.
        $item->fsitem_small_desc.'<br>'.$item->fsitem_passport_text.
      '</div>'.
    '</div>';
    endif; ?>
    <?php

    echo Tabs::widget ( [
      'encodeLabels' => false,
      'items' => [
        [
          'label' => '<span>Описание</span>',
          'content' => (isset($divpassport) ? $divpassport : '').'<div class="col-xs-12 padding-none description">'.$item->fsitem_bottom_text.'</div>',
          'active' => true,
        ],
        [
          'label' => '<span class="hidden-xs hidden-sm">Технические характеристики</span><span class="visible-xs visible-sm">Характеристики</span>',
          'content' => $tech,
        ],
        [
          'label' => '<span class="hidden-xs hidden-sm">Схема подключения</span><span class="visible-xs visible-sm">Схема</span>',
          'content' => $scheme,
        ],
      ]
    ] );

    ?>
  </div>
</div>

 <?php if ($item->similarItems): ?>
<style>
 .new-product {
    padding-top: 30px;
}

.evr-this {
    padding-bottom: 30px;
    padding-top: 50px;
    font-size: 24px;
    color: #213149;
}
.center-align {
    text-align: center;
}
.padding-none {
    padding: 0;
}
.owl-carousel {
    display: none;
    position: relative;
    width: 100%;
    -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper-outer {
    overflow: hidden;
    position: relative;
    width: 100%;
}
.catalogs-items .item-one {
    border: 1px solid #b2b2b2;
    min-height: 330px;
    margin-bottom: 20px;
}
.slide-elements .item-one .title {
    margin-bottom: -3px;
}

.item-one .title {
    margin-top: 5px;
    font-size: 16px;
    font-weight: bold;
    margin-bottom: 15px;
}
.slide-elements .cat-link {
    font-family: Arial;
    font-size: 12.82px;
    margin-bottom: 5px;
}
.item-one .price {
    margin-left: -5px;
}


.item-one .price {
    position: absolute;
    bottom: 70px;
    height: 30px;
}
.show-more-wrap {
    padding: 0 20px 0 0;
    position: absolute;
    bottom: 10px;
    width: 100%;

}
.catalogs-items .owl-item {
    padding: 0 5px;
}
.flex-direction-nav {
    position: relative;
    z-index: 100;
    float: right;
    right: 0;
    z-index: 0;
    margin-top: -45px;
    margin-right: 5px;
}
.slides, .slides>li, .flex-control-nav, .flex-direction-nav {
    margin: 0;
    padding: 0;
    list-style: none;
}
.flex-nav-prev, .flex-nav-next {
    display: inline;
    display: inline-block;
}
.flex-direction-nav span.btton {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #7b7b7b;
    color: #7b7b7b;
    display: inline-block;
    height: 34px;
    text-align: center;
    text-transform: uppercase;
    transition: all .2s ease-out 0s;
    width: 34px;
    font-family: sans-serif;
    font-weight: bold;
    line-height: 30px;
}
.flex-direction-nav span.btton:hover {
    background: #00e086 none repeat scroll 0 0;
    border: 2px solid #06a35c;
    color: #fff;
}

/*.col-md-4 {
    width: 28.333333%;
}*/

.similar_products .item-one {
    float: none;
    display: inline-block;
    margin-right: -13px;
}
.similar_products .slide-elements{
    text-align:center;
}

.item-one{
  margin: 0 10px;
}
.view-one .price-cell {
     padding-top: 0px;
}
</style>
<style>

  /*
 *  Core Owl Carousel CSS File
 *  v1.3.3
 */

/* clearfix */
.owl-carousel .owl-wrapper:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
/* display none until init */
.owl-carousel{
  display: none;
  position: relative;
  width: 100%;
  -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper{
  display: none;
  position: relative;
  -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer{
  overflow: hidden;
  position: relative;
  width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight{
  -webkit-transition: height 500ms ease-in-out;
  -moz-transition: height 500ms ease-in-out;
  -ms-transition: height 500ms ease-in-out;
  -o-transition: height 500ms ease-in-out;
  transition: height 500ms ease-in-out;
}

.owl-carousel .owl-item{
  float: left;
}
.owl-controls .owl-page,
.owl-controls .owl-buttons div{
  cursor: pointer;
}
.owl-controls {
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

/* mouse grab icon */
.grabbing {
    cursor:url(grabbing.png) 8 8, move;
}

/* fix */
.owl-carousel  .owl-wrapper,
.owl-carousel  .owl-item{
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility:    hidden;
  -ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0,0,0);
  -moz-transform: translate3d(0,0,0);
  -ms-transform: translate3d(0,0,0);
}

</style>
<style>
  /*
*   Owl Carousel Owl Demo Theme
* v1.3.3
*/

.owl-theme .owl-controls{
  margin-top: 10px;
  text-align: center;
}

/* Styling Next and Prev buttons */

.owl-theme .owl-controls .owl-buttons div{
  color: #FFF;
  display: inline-block;
  zoom: 1;
  *display: inline;/*IE7 life-saver */
  margin: 5px;
  padding: 3px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
  background: #869791;
  filter: Alpha(Opacity=50);/*IE7 fix*/
  opacity: 0.5;
}
/* Clickable class fix problem with hover on touch devices */
/* Use it for non-touch hover action */
.owl-theme .owl-controls.clickable .owl-buttons div:hover{
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
  text-decoration: none;
}

/* Styling Pagination*/

.owl-theme .owl-controls .owl-page{
  display: inline-block;
  zoom: 1;
  *display: inline;/*IE7 life-saver */
}
.owl-theme .owl-controls .owl-page span{
  display: block;
  width: 12px;
  height: 12px;
  margin: 5px 7px;
  filter: Alpha(Opacity=50);/*IE7 fix*/
  opacity: 0.5;
  -webkit-border-radius: 20px;
  -moz-border-radius: 20px;
  border-radius: 20px;
  background: #869791;
}

.owl-theme .owl-controls .owl-page.active span,
.owl-theme .owl-controls.clickable .owl-page:hover span{
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
}

/* If PaginationNumbers is true */

.owl-theme .owl-controls .owl-page span.owl-numbers{
  height: auto;
  width: auto;
  color: #FFF;
  padding: 2px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}

/* preloading images */
.owl-item.loading{
  min-height: 150px;
  background: url(AjaxLoader.gif) no-repeat center center
}
@media (max-width: 425){
.similar_products .item-one {
    margin-right: 0px;
}
.item-one {
     margin: 0;
}
}
</style>
<div class="inner_width similar_products">
    <div class="container padding-none ">
      <div class="col-xs-12 padding-none new-product evr-this center-align">
      <?=Yii::t('app', 'Похожие товары')?>
      </div>
      <p class="visible-xs">&nbsp;</p>
      <div class="col-xs-12 padding-none catalogs-items slider" style="margin-top: 20px;">
        <div class="slide-elements" id="owl-newitem">
        <?php foreach($item->similarItems as $similar): ?>
          <?php if(!empty($similar['fsitem_before_text'])): ?>
            <div class="col-xs-12 padding-none sub-item-desc">
              <?php echo $similar['fsitem_before_text'] ?>
            </div>
          <?php endif; ?>
          <?php echo $this->render('/catalog/item-similar', [
            'item' => $similar,
            'prices' => $prices,
            'cats' => [],//$cats,
            'page' => 'general'
          ]) ?>
        <?php endforeach; ?>
        </div>
      </div>
    </div>
</div>
<?php endif; ?>

  <div class="col-xs-12 padding-none gray-line"></div>
  <div class="col-xs-12 padding-none description"><?php //php echo $item->fsitem_bottom_text ?></div>

</div>
