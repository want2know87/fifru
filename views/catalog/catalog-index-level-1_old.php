<?php

use yii\helpers\Html;
use app\models\CatalogRu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subcat int */

/** @var CatalogRu $catInfo */
$catInfo = CatalogRu::find()->where(['ficatalog_id' => $subcat])->andWhere(['fivisible' => 1])->one();

$translates = json_decode($catInfo->translates, true);

$this->title = $translates['values'][Yii::$app->language]['fsname'];

// print_r($items);

$js = <<< JS

$(function() {

});

JS;

$js = str_replace('%URL%', \yii\helpers\Url::to(['catalog/get-item']), $js);

$this->registerJs($js);

$podcats = CatalogRu::find()->where(['fiparent_catalog_id' => $catInfo->ficatalog_id])->andWhere(['fivisible' => 1])->orderBy('fiorder')->all();

?>
<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>

<?php
    $modal = \yii\bootstrap\Modal::begin(['id' => 'show-info']);
    $modal->end();
?>
<div class="catalog-subcats">
    <div class="col-xs-12 padding-none general-titles center-block">
        <h2><?php echo $this->title ?></h2>
    </div>
    <div class="col-xs-12 padding-none catalogs-podcats">
        <?php foreach ($podcats as $pc): ?>
            <?php

            $translates = json_decode($pc->translates,true);

            ?>
            <?php /** @var CatalogRU $pc */ ?>
            <div class="col-xs-4 padding-none podcat-one">
                <div class="title col-xs-12 padding-none"><?php echo Html::a($translates['values'][Yii::$app->language]['fsname'], ['catalog/index', 'cat' => $pc->ficatalog_id]); ?></div>
                <div class="col-xs-4 padding-none image"><?php if(strlen($pc->fscatalog_img)) echo Html::img(Yii::$app->request->baseUrl . '/' . $pc->fscatalog_img) ?></div>
                <div class="col-xs-8 text"><?php echo $translates['values'][Yii::$app->language]['fscatalog_text'] ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
