<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\components\SiteView $this */
/** @var array $catalogs[] */
/** @var int $active */

?>
<div class="col-xs-12 padding-none general-titles">
    <?php foreach ($catalogs as $row): ?>
        <div class="col-md-2 col-xs-6  col-sm-4 padding-none big-cats <?php if($active == $row['id']) { echo "active"; } ?>">
            <div class="col-xs-12 padding-none image">
                <?php echo Html::tag( strlen($row['link']) ? 'noindex' : 'span', Html::a(Html::img(Yii::$app->request->baseUrl . '/' . $row['big-image']), strlen($row['link']) ? $row['link'] : Url::to(['catalog/index','cat' => $row['id']]), strlen($row['link']) ? ['target' => 'blank','rel'=>'nofollow'] : [])) ?>
            </div>
            <div class="col-xs-12 padding-none">
                <?php echo strlen($row['link']) ? Html::tag('noindex',Html::a($row['name'], $row['link'], ['target' => 'blank','rel'=>'nofollow'])) : Html::a($row['name'], Url::to(['catalog/index','cat' => $row['id']])) ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>