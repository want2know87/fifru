<?php

use yii\helpers\Html;
use app\models\CatalogRu;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatalogSearchRu */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subcat int */

$prices = ['BYR', 'RUR', 'USD'];

$js = <<<JS
  $(function() {

    var el = $("#filters");
    var start = el.offset();

    var fixedFilters = function() {
      if($('.items-elemets').height() > window.pageYOffset) {
        el.css('margin-top', (0 > (window.pageYOffset-start.top) ? 0 : (window.pageYOffset-start.top)) + "px");
      }
    };

    window.addEventListener("scroll", fixedFilters, false);
    fixedFilters();

    var currentFilters = {};

    var filterFunction = function() {
      $('.catalog-new-subcats .item-new-one').each(function() {
        var show = true;
        var item = $(this);
        var params = JSON.parse(item.find('.hidden-info').html());

        for(var index in currentFilters) {
          if(currentFilters[index] == null) {
            continue;
          }
          if(index == 'sale' && currentFilters[index] == true) {
            show = item.hasClass('sale');
            if(!show) {
              break;
            }
            continue;
          }
          if(index == 'finew' && currentFilters[index] == true) {
            show = item.hasClass('finew');
            if(!show) {
              break;
            }
            continue;
          }
          if(params == null || !params.hasOwnProperty(index) || params[index] != currentFilters[index]) {
            show = false;
            break;
          }
        }

        if(show) {
          item.show();
        }
        else {
          item.hide();
        }

      });
      checkFilters();
      $('html, body').animate({
        scrollTop: $('.catalog-new-subcats').offset().top
      }, 500);
    };

    var checkFilters = function () {
      var filters = $('.filter .one-filter:not(.sale):not(.reset)');
      filters.find('button').removeClass('disabled');
      filters.find('li').removeClass('disabled');
      var elements = $('.catalog-new-subcats .item-new-one:visible');
      filters.each(function() {
        var self = $(this);
        var id = self.find('button').data('filter');
        if(currentFilters.hasOwnProperty(id) && currentFilters[id] != null) {
          return true;
        }
        else {
          var findedElements = [];
          elements.each(function() {
            var element = $(this);
            var params = JSON.parse(element.find('.hidden-info').html());
            if(params.hasOwnProperty(id)) {
              findedElements.push(params[id]);
            }
          });
          if(findedElements.length) {
            self.find('ul li:not([data-id=null])').addClass('disabled');
            for(var i = 0; i< findedElements.length; i++) {
              self.find('ul li:contains("'+ findedElements[i] + '")').removeClass('disabled');
            }
          }
          else {
            self.find('button').addClass('disabled');
          }
        }
      });
    };

    $('.filter .reset ').on('click', 'span', function() {
      $('.filter .one-filter').each(function() {
        var el = $(this);
        el.find('.dropdown-menu').find('li:first').click();
        if(el.find('input[type=checkbox]').length && el.find('input[type=checkbox]').is(':checked')) {
          el.find('input[type=checkbox]').click();
        }
      });
    });

    $('.filter .sale ').on('click', 'input[name=sale]', function() {
      if($(this).is(':checked')) {
        currentFilters['sale'] = true;
      }
      else {
        currentFilters['sale'] = null;
      }
      filterFunction();
    });

    $('.filter .sale ').on('click', 'input[name=finew]', function() {
      if($(this).is(':checked')) {
        currentFilters['finew'] = true;
      }
      else {
        currentFilters['finew'] = null;
      }
      filterFunction();
    });

    $('.filters .filter').on('click', '.btn-group button', function() {
      var el = $(this).find('.caret');
        el.stop( true, true ).fadeOut("fast").stop( true, true ).fadeIn("fast");
    });

    $('.filter .dropdown-menu').on('click', 'li:not(.disabled)', function() {
      var self = $(this);
      var filterButton = self.closest('.btn-group').find('button:first');
      var filterId = filterButton.data('filter');
      currentFilters[filterId] = (self.data('id') == null ? null : self.text());
      filterButton.text('');
      filterButton.html(self.text() + '<span class="caret"></span>');
      filterFunction();
      // console.log(currentFilters);
    });

  });
JS;

\yii\web\JqueryAsset::register($this);
$this->registerJs($js);

/** @var CatalogRu $catInfo */
$catInfo = CatalogRu::find()->where(['ficatalog_id' => $subcat])->andWhere(['fivisible' => 1])->one();

// if($catInfo->ficatalog_id == 41) {
//     require_once 'catalog-41.php';
//     return;
// }

$this->title = $catInfo->fsname;
$this->seoFields = $catInfo->fsseo;

$podcats = Yii::$app->db->createCommand('SELECT * FROM catalog_ru WHERE fiparent_catalog_id = :parentid ORDER BY fiorder', ['parentid' => $subcat])->queryAll();
$podcatsIds = ArrayHelper::map($podcats, 'ficatalog_id', 'fsname');

$items = Yii::$app->db->createCommand('SELECT
                                            i.*, s.fssale_name
                                        FROM
                                            items i
                                        LEFT JOIN sales s ON i.fisale_id = s.fisale_id
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id
                                        LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id*/
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id IN (' . implode(',', array_merge([(int)$subcat], array_keys($podcatsIds))) . ')
                                            )
                                        ORDER BY fiorder')->queryAll();

$itemsIds = ArrayHelper::map($items,'fiitem_id', 'fsitem_name');

$itemsValues = (new yii\db\Query())
  ->select(['pv.fiitem_id', 'pv.fiparam_id', 'pc.fsparam_name', 'pv.fsparam_value'])
  ->from('params_values pv')
  ->leftJoin('params_cat pc', 'pc.fiparam_id = pv.fiparam_id')
  ->where(['pv.fiitem_id' => array_keys($itemsIds)])
  ->orderBy('pv.fiorder')
  ->all(Yii::$app->db);

$itemsValues = ArrayHelper::map($itemsValues, 'fiparam_id', function($el) { return [$el['fsparam_name'] => $el['fsparam_value']]; }, 'fiitem_id');

$podItems = [];
$searchAr = [0];

$filtersId = $catInfo->filters;
if(empty($filtersId)) {
  $filtersId = [0];
}
else {
  $filtersId = json_decode($filtersId, true);
}

if(empty($filtersId)) {
  $filtersId = [0];
}

$catsIds = CatalogRu::find()->where(['fiparent_catalog_id' => (int)$catInfo->ficatalog_id])->all();
$catsIds = \yii\helpers\ArrayHelper::map($catsIds, 'ficatalog_id', 'fsname');
$catsIds = array_merge(
  [(int)$catInfo->ficatalog_id],
  array_keys($catsIds)
);

$filters = Yii::$app->db->createCommand('SELECT DISTINCT
	pc.fiparam_id as `param_id`, pc.fsparam_name AS `name`
FROM
	params_values pv
LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
WHERE
	pv.fiitem_id IN(
		SELECT
			fiitem_id
		FROM
			catalog_items
		WHERE
			ficatalog_id IN (' . implode(',', $catsIds) . ')
	)
	AND
	pc.fiparam_id IN (' . implode(', ', $filtersId) . ')
ORDER BY
	(
		CASE
		WHEN pv.fiorder IS NULL THEN
			99999
		ELSE
			pv.fiorder
		END
	)')->queryAll();

$values = Yii::$app->db->createCommand('SELECT DISTINCT
	pv.fiparam_value_id `value_id`, pv.fiparam_id as `param_id`, pv.fsparam_value as `value`
FROM
	params_values pv
LEFT JOIN params_cat pc ON pv.fiparam_id = pc.fiparam_id
WHERE
	pv.fiitem_id IN(
		SELECT
			fiitem_id
		FROM
			catalog_items
		WHERE
			ficatalog_id IN (' . implode(',', $catsIds) . ')
	)
	GROUP BY pv.fiparam_id,fsparam_value
ORDER BY
	(
		CASE
		WHEN pv.fiorder IS NULL THEN
			99999
		ELSE
			pv.fiorder
		END
	)')->queryAll();

$values = ArrayHelper::map($values, 'value_id', 'value', 'param_id');
$filters = ArrayHelper::map($filters, 'param_id', 'name');

foreach($podcats as $pod) {
    $podItems[$pod['ficatalog_id']] = [
        'id'       => $pod['ficatalog_id'],
        'name'     => $pod['fsname'],
        'elements' => [],
    ];
    $searchAr[] = (int)$pod['ficatalog_id'];
}

$itemsPodcats = Yii::$app->db->createCommand('SELECT DISTINCT
                                            i.*, ci.ficatalog_id as cat
                                        FROM
                                            items i
                                        /*LEFT JOIN params_values pv ON pv.fiitem_id = i.fiitem_id*/
                                        INNER JOIN catalog_items ci ON i.fiitem_id = ci.fiitem_id
                                        WHERE
                                            i.fiitem_id IN(
                                                SELECT
                                                    fiitem_id
                                                FROM
                                                    catalog_items
                                                WHERE
                                                    ficatalog_id IN (' . implode(',', $searchAr) . ')
                                            )
                                        ORDER BY fiorder')->queryAll();

foreach($itemsPodcats as $item) {
    $podItems[$item['cat']]['elements'][] = $item;
}

?>
<?php echo $this->render('/catalog/breadcrumb', ['breadcrumb' => $breadCrumbs]) ?>



<div class="catalog-new-subcats">
    <div class="col-xs-12 padding-none general-titles center-block">
        <h1><?php echo $this->title ?></h1>
    </div>
    <?php if(strlen($catInfo->fsdestination) || strlen($catInfo->fswork) || strlen($catInfo->fsuse)): ?>
      <div class="col-xs-12 padding-none cat-desc" id="accordion">
        <?php if(strlen($catInfo->fsdestination)): ?>

          <div class="col-xs-12 padding-none title"><span class="dotted-link">Назначение</span></div>
          <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fsdestination ?></div>

        <?php endif; ?>
        <?php if(strlen($catInfo->fswork)): ?>

          <div class="col-xs-12 padding-none title"><span class="dotted-link">Принцип работы</span></div>
          <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fswork ?></div>

        <?php endif; ?>
        <?php if($catInfo->fsuse): ?>

          <div class="col-xs-12 padding-none title"><span class="dotted-link">Применение</span></div>
          <div class="col-xs-12 padding-none desc"><?php echo $catInfo->fsuse ?></div>

        <?php endif; ?>
      </div>
    <?php endif; ?>

    <div class="col-xs-12 padding-none catalogs-items-new">
        <div id="filters" class="col-sm-3 padding-none filters">
            <?=$this->render('_test-form')?>
            <div class="col-xs-12 padding-none filter">
                <div class="padding-none col-xs-12 title">Отфильтруйте продукцию<br/>по техническим параметрам</div>
                <?php foreach($filters as $id => $filter): ?>
                  <?php
                    if(empty($values[$id])) {
                      continue;
                    }
                    else {
                      $values[$id] = ['null' => 'Не выбрано'] + $values[$id];
                    }
                  ?>
                    <div class="padding-none col-xs-12 one-filter">
                        <label for="catalog-filter-<?php echo $id ?>"><?php echo $filter; ?></label>
                        <?php
                            echo ButtonDropdown::widget ( [
                              'label' => @array_values(@$values[$id])[0],
                              /*'split' => true,*/
                              'options' => [
                                'class' => 'btn-lg btn-default',
                                'data-active' => @array_keys(@$values[$id])[0],
                                'data-filter' => $id,
                              ],
                              'dropdown' => [
                                'items' => array_map(
                                  function($el, $key) {
                                      return ['label' => $el, 'options' => ['data-id' => $key]];
                                  } ,@$values[$id], array_keys(@$values[$id])),
                              ],
                            ] );
                        ?>
                    </div>
                    <?php ?>
                <?php endforeach; ?>
              <div class="padding-none sale col-xs-12 one-filter">
                <input id="catalog-filter-sale" type="checkbox" name="sale" />
                <label for="catalog-filter-sale">товары на скидке</label>
              </div>
              <div class="padding-none sale finew col-xs-12 one-filter">
                <input id="catalog-filter-finew" type="checkbox" name="finew" />
                <label for="catalog-filter-finew">новинки</label>
              </div>
              <div class="padding-none reset col-xs-12 one-filter">
                <span>Сбросить фильтры</span>
              </div>
            </div>
        </div>
        <div class="col-sm-9 items-elemets">
            <?php foreach($podItems as $podcats): ?>
                <?php //$items = array_merge($items, $podcats['elements']); ?>
            <?php endforeach; ?>
            <?php $items = array_map("unserialize", array_unique(array_map("serialize", $items))); ?>
            <?php foreach($items as $item): ?>
                <?php /*if(!empty($item['fsitem_before_text'])): */?><!--
                    <div class="col-xs-12 padding-none sub-item-desc">
                        <?php /*echo $item['fsitem_before_text'] */?>
                    </div>
                --><?php /*endif; */?>
                <?php echo $this->render('item-new', ['item' => $item, 'prices' => $prices, 'itemsValues' => $itemsValues, 'breadCrumbs' => $breadCrumbs]) ?>
            <?php endforeach; ?>
          <div class="col-xs-12 padding-none">
            <?php echo $catInfo->fscatalog_bottom_text?>
          </div>

        </div>
    </div>
</div>
