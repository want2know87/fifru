<div class="col-xs-<?php echo empty($social) ? '12' : '12' ?> padding-none breadcrumbs">
    <?php if(!empty($breadcrumb['general_name'])): ?>
        <div class="general-title">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/index','cat' => $breadcrumb['general_id']]) ?>"><?php echo $breadcrumb['general_name'] ?></a>
        </div>
    <?php endif; ?>
    <?php if(!empty($breadcrumb['middle_name'])): ?>
        <div class="middle-title">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/index','cat' => $breadcrumb['middle_id']]) ?>"><?php echo $breadcrumb['middle_name'] ?></a>
        </div>
    <?php endif; ?>

    <?php if(isset($breadcrumb['sublow_name']) && !empty($breadcrumb['sublow_name'])): ?>
        <div class="middle-title">
            <a href="<?php echo \yii\helpers\Url::to(['catalog/index','cat' => $breadcrumb['sublow_id']]) ?>"><?php echo $breadcrumb['sublow_name'] ?></a>
        </div>
    <?php endif; ?>
    
    <?php if(!empty($breadcrumb['low_name'])): ?>
        <div class="low-title">
            <span><?php echo $breadcrumb['low_name'] ?></span>
        </div>
    <?php endif; ?>
</div>
<?php if(!empty($social)): ?>
  <?php
    $this->registerJsFile('@web/js/social-likes.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
    $this->registerCssFile('@web/js/social-likes_flat.css');
    ?>
  <div class="col-xs-3 padding-none social-top">
      <div class="social-likes social-likes_light" data-counters="no" data-url="<?php echo $social['link'] ?>" data-title="<?php echo \yii\helpers\Html::encode($social['title']) ?>">
          <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
          <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
          <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках"></div>
      </div>
  </div>
<?php endif; ?>