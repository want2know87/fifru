<?php

use app\models\SiteTemplates;

/** @var SiteTemplates $template */
$template = SiteTemplates::findOne(['fscode' => 'news/index']);
$this->title = $template->fstitle;
$this->seoFields = $template->fstemplate_seo;
eval("?>" . $template->fstemplate_code . "<?php ");
return;

use yii\helpers\Html;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */

$years = Yii::$app->db->createCommand('SELECT DISTINCT YEAR(fdcreate_date) year FROM news order by 1 desc')->queryAll();

?>

<div class="col-xs-12 padding-none about-title">
  <h2><?php echo Yii::t('app', 'news.title') ?></h2><br/>
</div>
<div class="col-xs-12 padding-none another-text news-years">
  <div class="center-block">
    <?php foreach($years as $year): ?>
      <a href="<?php echo \yii\helpers\Url::to(['news/index', 'year' => $year['year']]) ?>">
      <span class="year <?php if((@$_GET['year'] == $year['year']) || count($years) == 1) echo " active" ?>"><?php echo $year['year'] ?></span>
      </a>
    <?php endforeach; ?>
  </div>
</div>

<?php

$query = \app\models\News::find()->orderBy(['fdcreate_date' => SORT_DESC]);
if(isset($_GET['year'])) {
  $query->where(['YEAR(fdcreate_date)' => (int)$_GET['year']]);
}

$provider = new ActiveDataProvider([
  'query' => $query,
  'pagination' => [
    'pageSize' => 7,
  ],
]);


echo \yii\widgets\ListView::widget([
  'dataProvider' => $provider,
  'itemView' => 'view',
  'options' => ['class' => 'list-view'],
  'itemOptions' => [
    'class' => 'block-item',
  ],
  'summaryOptions' => [
    'style' => 'display:none;'
  ],
  'pager'=> [
    /*'header' => 'Страницы: ',*/
    'prevPageLabel' => false,
    'nextPageLabel' => false,
  ],
]);

?>