<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->registerJsFile('@web/js/social-likes.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerCssFile('@web/js/social-likes_flat.css');

?>
<div class="news-view">

  <div class="col-xs-12 padding-none">
    <div class="col-sm-3 hidden-xs padding-none first-block">
      <div class="news-date"><?php echo date('d.m.Y', strtotime($model->fdcreate_date)) ?></div>
      <?php if($model->fsimage_news): ?>
        <?php echo Html::img(Yii::$app->request->baseUrl . '/' . $model->fsimage_news, ['style' => 'max-width:154px;max-height:154px;']) ?>
      <?php endif; ?>
    </div>
    <div class="col-sm-9 col-xs-12 padding-none">
      <div class="news-title"><?php echo \yii\helpers\Html::a($model->fstitle, ['news/view', 'id' => $model->finewsid]) ?></div>
      <div class="news-anons"><?php echo $model->fsanons ?></div>
      <div class="news-likes">
        <div class="social-likes social-likes_light" data-counters="no" data-url="<?php echo \yii\helpers\Url::to(['news/view', 'id' => $model->finewsid]) ?>" data-title="<?php echo htmlspecialchars($model->fstitle) ?>">
          <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
          <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
          <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
        </div>
      </div>
      <div class="news-more"><?php echo \yii\helpers\Html::a('читать подробнее', ['news/view', 'id' => $model->finewsid]) ?></div>
    </div>
  </div>

</div>
