<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model \app\models\News */

$this->title = $model->fstitle;

$years = Yii::$app->db->createCommand('SELECT DISTINCT YEAR(fdcreate_date) year FROM news')->queryAll();

$this->registerCssFile('@web/js/ckeditor/contents.css');
$this->registerJsFile('@web/js/social-likes.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerCssFile('@web/js/social-likes_flat.css');

?>

<div class=" about-title news-one">
  <h2><a href="<?php echo \yii\helpers\Url::to(['news/index']) ?>"><?php echo Yii::t('app', 'news.title') ?></a></h2><br/>
</div>

<div class=" news-one">

  <div class="col-xs-12 news-date padding-none">
    <div class="news-date"><?php echo date('d.m.Y', strtotime($model->fdcreate_date)) ?></div>
  </div>

  <div class="col-xs-12 news-title padding-none">
    <div class="news-title"><?php echo $model->fstitle ?></div>
  </div>

  <div class="col-xs-12 news-text padding-none">
    <div class="news-text"><?php echo $model->fstext ?></div>
  </div>

</div>

<?php
/*
$another = \app\models\News::find()->where(['not in', 'finewsid', [$model->finewsid]]);
if((int)@$_GET['year']) {
  $another->where(['YEAR(fdcreate_date)' => (int)$_GET['year']]);
}

$another = $another->orderBy(['fdcreate_date' => 'desc'])->limit(5)->all();
*/
$another = \app\models\News::find()->orderBy('fdcreate_date desc')->limit(5)->all();

?>

<?php /* ?>
<div class="news-likes col-xs-12 news-like">
  <div class="social-likes social-likes_light" data-counters="no" data-url="<?php echo \yii\helpers\Url::to(['news/view', 'id' => $model->finewsid]) ?>" data-title="<?php echo htmlspecialchars($model->fstitle) ?>">
    <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
    <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
    <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
  </div>
</div>
<?php */ ?>


<div class="col-xs-12 another-news">
  <div class="another-title">Другие новости:</div>
  <?php foreach($another as $news): ?>
    <div class="row">
      <div class="col-sm-1 date"><?php echo  date('d.m.Y', strtotime($news['fdcreate_date']))  ?></div>
      <div class="col-sm-11 title"><a href="<?php echo \yii\helpers\Url::to(['news/view','id' => $news['finewsid']]) ?>"><?php echo $news['fstitle'] ?></a></div>
    </div>
  <?php endforeach; ?>
</div>