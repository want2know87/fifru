<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
?>
<h1>Административная панель</h1>

<p>
    <?php echo Html::a('Управление новостями', ['news/admin']) ?><br/>
    <?php echo Html::a('Управление каталогом', ['catalog/admin']) ?><br/>
    <?php echo Html::a('Управление товарами', ['items/admin']) ?><br/>
    <?php echo Html::a('Управление дистрибьюторами', ['distributors/admin']) ?><br/>
    <?php echo Html::a('Управление сертификатами', ['certificats/admin']) ?><br/>
    <?php echo Html::a('Управление странами', ['country/index']) ?><br/>
    <?php echo Html::a('Управление городами', ['town/index']) ?><br/>
    <?php echo Html::a('Управление публикациями', ['publications/admin']) ?><br/>
    <?php echo Html::a('Управление партнерами', ['partners/admin']) ?><br/>
    <?php echo Html::a('Управление шаблонами сайта', ['site-templates/index']) ?><br/>
    <?php echo Html::a('Управление текстами сайта', ['source-message/index']) ?><br/>
    <?php echo Html::a('Управление ЧПУ', ['chpu/index']) ?>
    <?php echo Html::a('Обновить и сгенерировать новый .htaccess', ['admin/updatechpu'], ['class' => 'btn btn-primary btn-xs', 'data' => ['method' => 'post']]) ?>
    <br/>
    <?php echo Html::a('Шаблоны нестандартных каталогов', ['catalog-templates/index']) ?><br/>
    <?php echo Html::a('Управление скидками', ['sales/index']) ?><br/>

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]) ?>

    <div class="form-group field-items-fsitem_picture has-success">
        <label class="control-label" for="items-fsitem_picture">Закачать каталог</label>
        <input name="catalog" value="" type="hidden"><input id="items-fsitem_picture" name="catalog" type="file">
    </div>

    <button class="btn btn-default">Закачать</button>

    <div class="form-group field-items-fsitem_picture has-success">
        <label class="control-label" for="items-fsitem_picture">Закачать прайс</label>
        <input name="excel" value="" type="hidden"><input id="items-fsitem_picture" name="excel" type="file">
    </div>

    <button class="btn btn-default">Закачать</button>

    <?php $form->end(); ?><br/>

    <?php echo Html::a('Выход', ['site/logout']) ?><br/>

</p>
