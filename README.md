tde-fif.ru
==========

Локально развернуть
```mkdir ~/sites/fif/public_html/old-fifru```
```ln -s ~/sites/fif/repo/fifru/web ~/sites/fif/public_html/old-fifru/web```

Логин и пароль для админа задавать в файле ```/config/admin.php```

Файлы и папки заигнорены:
- /web/index.php
- /web/*.xls
- /web/*.xlsx
- /web/*.docx
- /web/*.pdf
- /web/*.txt
- /web/data/
- /web/image/
- /web/images/
